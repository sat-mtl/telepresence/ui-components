/* global expect beforeEach jest describe it */

import React from 'react'
import { shallow } from 'enzyme'

import Rail from '~/inputs/Slider/Rail.js'

describe('<Rail />', () => {
  let setInsideStateMock, setDownStateMock
  let onSlide

  beforeEach(() => {
    onSlide = jest.fn()
  })

  function initHookMocks (initInside, initDown) {
    setInsideStateMock = jest.fn()
    setDownStateMock = jest.fn()

    jest.spyOn(React, 'useState')
      .mockImplementationOnce((init) => [initInside || init, setInsideStateMock])
      .mockImplementationOnce((init) => [initDown || init, setDownStateMock])
  }

  const getRail = (disabled = false) => shallow(
    <Rail
      disabled={disabled}
      onSlide={onSlide}
    />
  )

  describe('onMouseMove', () => {
    it('should slide if the mouse is inside the rail and pressed', () => {
      const $rail = getRail()
      $rail.simulate('mousedown')
      $rail.simulate('mouseenter')
      $rail.simulate('mousemove')
      expect(onSlide).toHaveBeenCalled()
    })

    it('should not slide if the mouse is outside the rail', () => {
      const $rail = getRail()
      $rail.simulate('mousedown')
      $rail.simulate('mousemove')
      expect(onSlide).not.toHaveBeenCalled()
    })

    it('should not slide if the mouse is not pressed', () => {
      const $rail = getRail()
      $rail.simulate('mouseenter')
      $rail.simulate('mousemove')
      expect(onSlide).not.toHaveBeenCalled()
    })
  })

  describe('onMouseLeave', () => {
    it('should set the inside state to false', () => {
      initHookMocks()
      const $rail = getRail()
      $rail.simulate('mouseleave')
      expect(setInsideStateMock).toHaveBeenCalledWith(false)
    })

    it('should not reset the down if it is not pressed', () => {
      initHookMocks()
      const $rail = getRail()
      $rail.simulate('mouseleave')
      expect(setDownStateMock).not.toHaveBeenCalled()
    })

    it('should set the down state to false if it is pressed', () => {
      initHookMocks(undefined, true)
      const $rail = getRail()
      $rail.simulate('mouseleave')
      expect(setDownStateMock).toHaveBeenLastCalledWith(false)
    })

    it('should slide the latest value if the mouse is pressed', () => {
      const $rail = getRail()
      $rail.simulate('mousedown')
      $rail.simulate('mouseleave')
      expect(onSlide).toHaveBeenCalled()
    })

    it('should not slide the value if the mouse isn\'t pressed', () => {
      const $rail = getRail()
      $rail.simulate('mouseleave')
      expect(onSlide).not.toHaveBeenCalled()
    })
  })

  describe('onMouseEnter', () => {
    it('should set the mouse state to `inside`', () => {
      initHookMocks()
      const $rail = getRail()
      $rail.simulate('mouseenter')
      expect(setInsideStateMock).toHaveBeenCalledWith(true)
    })
  })

  describe('onMouseDown', () => {
    it('should set the mouse state to `pressed`', () => {
      initHookMocks()
      const $rail = getRail()
      $rail.simulate('mousedown')
      expect(setDownStateMock).toHaveBeenCalledWith(true)
    })

    it('should not slide if the mouse is outside', () => {
      initHookMocks()
      const $rail = getRail()
      $rail.simulate('mousedown')
      expect(onSlide).not.toHaveBeenCalled()
    })

    it('should slide if the mouse is inside', () => {
      initHookMocks(true)
      const $rail = getRail()
      $rail.simulate('mousedown')
      expect(onSlide).toHaveBeenCalled()
    })
  })

  describe('onMouseUp', () => {
    it('should set the mouse state to `not pressed`', () => {
      initHookMocks()
      const $rail = getRail()
      $rail.simulate('mouseup')
      expect(setDownStateMock).toHaveBeenCalledWith(false)
    })
  })
})
