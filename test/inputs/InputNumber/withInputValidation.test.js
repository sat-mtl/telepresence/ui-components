/* global describe it jest expect beforeEach */

import React from 'react'
import { shallow } from 'enzyme'

import InputNumber from '~/inputs/InputNumber/InputNumber.js'
import withInputValidation from '~/inputs/InputNumber/withInputValidation.js'

const InputValidator = withInputValidation(InputNumber)

describe('withInputValidation', () => {
  const MAX_VALUE = 100
  const MIN_VALUE = 0
  const INPUT_VALUE = 4
  const VALID_INPUT = { target: { value: INPUT_VALUE + 1 } }
  const INVALID_INPUT = { target: { value: undefined } }

  const shallowInputValidator = (onChange, value = MAX_VALUE) => {
    const $input = shallow(
      <InputValidator onChange={onChange} max={MAX_VALUE} min={MIN_VALUE} value={value} />
    )

    return [$input, $input.instance()]
  }

  describe('handleValidation', () => {
    it('should not handle validation when the received event is not well-formed', () => {
      const onChangeMock = jest.fn()
      const [, input] = shallowInputValidator(onChangeMock)
      input.handleValidation({})
      expect(onChangeMock).not.toHaveBeenCalled()
    })

    it('should call `onChange` when the input is validated', () => {
      const onChangeMock = jest.fn()
      const [, input] = shallowInputValidator(onChangeMock)
      input.handleValidation(VALID_INPUT)
      expect(onChangeMock).toHaveBeenCalled()
    })

    it('should not call `onChange` when the inputed value is out of bounds but should reset the input', () => {
      const ABUSIVE_INPUT = { target: { value: MAX_VALUE + 1 } }
      const onChangeMock = jest.fn()
      const [, input] = shallowInputValidator(onChangeMock, MAX_VALUE)
      input.handleValidation(ABUSIVE_INPUT)
      expect(onChangeMock).not.toHaveBeenCalled()
      expect(ABUSIVE_INPUT.target.value).toEqual(MAX_VALUE)
    })

    it('should call `onChange` with maximum value if the changed value is upper the maximum', () => {
      const ABUSIVE_INPUT = { target: { value: MAX_VALUE + 1 } }
      const onChangeMock = jest.fn()
      const [, input] = shallowInputValidator(onChangeMock, INPUT_VALUE)
      input.handleValidation(ABUSIVE_INPUT)
      expect(onChangeMock).toHaveBeenCalledWith(MAX_VALUE)
    })

    it('should call `onChange` with minimum value if the changed value is lesser the minimum', () => {
      const ABUSIVE_INPUT = { target: { value: MIN_VALUE - 1 } }
      const onChangeMock = jest.fn()
      const [, input] = shallowInputValidator(onChangeMock, INPUT_VALUE)
      input.handleValidation(ABUSIVE_INPUT)
      expect(onChangeMock).toHaveBeenCalledWith(MIN_VALUE)
    })

    it('should reset the input value when the input is invalid', () => {
      const onChangeMock = jest.fn()
      const [, input] = shallowInputValidator(onChangeMock, INPUT_VALUE)
      input.handleValidation(INVALID_INPUT)
      expect(onChangeMock).not.toHaveBeenCalled()
      expect(INVALID_INPUT.target.value).toEqual(INPUT_VALUE)
    })
  })

  describe('handleKeyDown', () => {
    let handleValidationSpy

    beforeEach(() => {
      handleValidationSpy = jest.spyOn(InputValidator.prototype, 'handleValidation')
    })

    it('should handle keyDown and call validation', () => {
      const [, input] = shallowInputValidator()
      input.handleKeyDown({ which: 13 })
      expect(handleValidationSpy).toHaveBeenCalled()
    })

    it('should not handle keyDown when the received event is not well-formed', () => {
      const [, input] = shallowInputValidator()
      input.handleKeyDown({})
      expect(handleValidationSpy).not.toHaveBeenCalled()
    })

    it('should not handle keyDown if the key pressed is not ENTER', () => {
      const [, input] = shallowInputValidator()
      input.handleKeyDown({ which: 1 })
      expect(handleValidationSpy).not.toHaveBeenCalled()
    })
  })
})
