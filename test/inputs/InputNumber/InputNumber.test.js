/* global jest describe it expect */

import React from 'react'
import { shallow, mount } from 'enzyme'

import InputNumber from '~/inputs/InputNumber/InputNumber.js'
import InputButton, { InputButtonDecrement, InputButtonIncrement } from '~/inputs/InputNumber/InputButton.js'

describe('<InputNumber />', () => {
  const getField = ($input) => $input.find('.InputNumberField')

  describe('render', () => {
    it('should render an InputNumber with two buttons and an editable numeric field', () => {
      const $input = mount(<InputNumber />)
      expect($input.find(InputButton)).toHaveLength(2)
      expect($input.find('input')).toHaveLength(1)
      expect($input.find('input').prop('value')).toEqual(0)
    })

    it('should render an InputNumber with a status', () => {
      const $input = shallow(<InputNumber status='danger' />)
      expect($input.hasClass('InputNumber-danger')).toEqual(true)
    })

    it('should not render buttons with `hideButtons` prop set to true', () => {
      const $input = mount(<InputNumber withButtons={false} />)
      expect($input.find(InputButton)).toHaveLength(0)
    })
  })

  describe('onIncrement', () => {
    it('should increment value when increment button is clicked', () => {
      const onChange = jest.fn()
      const $input = mount(<InputNumber value={0} onChange={onChange} />)
      $input.find(InputButtonIncrement).find('button').simulate('click')
      expect(onChange).toHaveBeenCalledWith(1)
    })

    it('should keep old value if inputed value is upper than max', () => {
      const onChange = jest.fn()
      const $input = mount(<InputNumber max={1} value={1} onChange={onChange} />)
      $input.find(InputButtonIncrement).find('button').simulate('click')
      expect(onChange).toHaveBeenCalledWith(1)
    })
  })

  describe('onDecrement', () => {
    it('should decrement value when decrement button is clicked', () => {
      const onChange = jest.fn()
      const $input = mount(<InputNumber value={0} onChange={onChange} />)
      $input.find(InputButtonDecrement).find('button').simulate('click')
      expect(onChange).toHaveBeenCalledWith(-1)
    })

    it('should keep old value if inputed value is lower than min', () => {
      const onChange = jest.fn()
      const $input = mount(<InputNumber min={-1} value={-1} onChange={onChange} />)
      $input.find(InputButtonDecrement).find('button').simulate('click')
      expect(onChange).toHaveBeenCalledWith(-1)
    })
  })

  describe('onChange', () => {
    it('should not call `onChange` value when the value is inputed', () => {
      const onChange = jest.fn()
      const $input = shallow(<InputNumber onChange={onChange} value={4} />)
      getField($input).simulate('change', { target: { value: 4 } })
      expect(onChange).not.toHaveBeenCalledWith(4)
    })

    it('should change internal value when something is inputed', () => {
      const onChange = jest.fn()
      const $input = shallow(<InputNumber onChange={onChange} />)
      getField($input).simulate('change', { target: { value: 4 } })
      expect(getField($input).prop('value')).toEqual(4)
    })

    it('should only change inputed value when crap is inputed', () => {
      const onChange = jest.fn()
      const $input = shallow(<InputNumber onChange={onChange} value={4} />)
      getField($input).simulate('change', { target: { value: 'test' } })
      expect(getField($input).prop('value')).toEqual('test')
      expect(onChange).not.toHaveBeenCalledWith('test')
    })

    it('should let user input an out of range value', () => {
      const onChange = jest.fn()
      const $input = shallow(<InputNumber onChange={onChange} min={-1} max={1} />)
      getField($input).simulate('change', { target: { value: 4 } })
      expect(getField($input).prop('value')).toEqual(4)
    })
  })

  describe('onBlur', () => {
    it('should call `onBlur` when the input is blurred', () => {
      const onBlur = jest.fn()
      const $input = shallow(<InputNumber onBlur={onBlur} value={4} />)
      getField($input).simulate('blur')
      expect(onBlur).toHaveBeenCalled()
    })
  })

  describe('onClick', () => {
    it('should select the input field when it is clicked', () => {
      const mockSelect = jest.fn()
      const $input = shallow(<InputNumber />)
      getField($input).simulate('click', { target: { select: mockSelect } })
      expect(mockSelect).toHaveBeenCalled()
    })
  })
})
