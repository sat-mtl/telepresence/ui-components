/* global describe it expect */

import React from 'react'
import { shallow, mount } from 'enzyme'

import InputPassword from '~/inputs/InputPassword'
import InputText from '~/inputs/InputText'
import Button from '~/common/Button'
import Icon from '~/common/Icon'

describe('<InputPassword />', () => {
  describe('InputText', () => {
    it('should contain an InputText', () => {
      const $input = shallow(<InputPassword />)
      expect($input.find(InputText).length).toEqual(1)
    })
  })

  describe('getToggleIconType', () => {
    it('should mute icon if it is toggled', () => {
      const $pw = mount(<InputPassword />)
      const $icon = $pw.find(Icon)
      expect($icon.prop('type')).toEqual('eye')
    })

    it('should display an icon by default', () => {
      const $pw = mount(<InputPassword />)
      const $toggle = $pw.find(Button)
      $toggle.simulate('click', {})
      const $icon = $pw.find(Icon)
      expect($icon.prop('type')).toEqual('eye-blind')
    })
  })

  describe('getPasswordProps', () => {
    const getInputText = ($pw) => $pw.find(InputText)

    it('should be an input with type password by default', () => {
      const $pw = shallow(<InputPassword />)
      const $input = $pw.find(InputText)
      expect($input.prop('inputProps').type).toEqual('password')
    })

    it('should be an input with type `text` if the `display` switch is clicked', () => {
      const $pw = mount(<InputPassword />)
      const $toggle = $pw.find(Button)
      $toggle.simulate('click', {})
      expect(getInputText($pw).prop('inputProps').type).toEqual('text')
    })

    it('should not toggle input type if it is disabled', () => {
      const $pw = mount(<InputPassword disabled />)
      expect(getInputText($pw).prop('inputProps').type).toEqual('password')
      const $toggle = $pw.find(Button)
      $toggle.simulate('click', {})
      expect(getInputText($pw).prop('inputProps').type).toEqual('password')
    })
  })
})
