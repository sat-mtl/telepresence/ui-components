/* global jest describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Checkbox from '~/inputs/Checkbox'

describe('<Checkbox />', () => {
  it('should render a Checkbox', () => {
    const $box = shallow(<Checkbox label='test' />)
    expect($box.find('.Checkbox')).toHaveLength(1)
  })

  it('should handle click has a check', () => {
    const changeSpy = jest.fn()
    const $box = shallow(<Checkbox onChange={changeSpy} />)
    $box.simulate('click')
    expect(changeSpy).toHaveBeenCalled()
  })

  it('should prevent change when disabled', () => {
    const changeSpy = jest.fn()
    const $box = shallow(<Checkbox disabled onChange={changeSpy} />)
    $box.simulate('click')
    expect(changeSpy).not.toHaveBeenCalled()
  })
})
