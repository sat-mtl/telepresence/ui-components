/* global jest describe it expect beforeEach */

import React from 'react'
import { shallow, mount } from 'enzyme'

import InputText, { Addon } from '~/inputs/InputText/InputText'

describe('<InputText />', () => {
  describe('enabled', () => {
    it('should render a label if the inputText size is large', () => {
      const $input = mount(<InputText size='large' />)
      expect($input.find('label')).toHaveLength(1)
    })

    it('should not render a label if the inputText size is normal', () => {
      const $input = mount(<InputText size='normal' />)
      expect($input.find('label')).toHaveLength(0)
    })

    it('should render an InputText', () => {
      const $input = shallow(<InputText />)
      expect($input.find('input')).toHaveLength(1)
    })

    it('should select all content on focus', () => {
      const mockSelect = jest.fn()
      const $input = shallow(<InputText />)

      $input.find('input').prop('onFocus')({
        target: { select: mockSelect }
      })

      expect(mockSelect).toHaveBeenCalled()
    })

    it('should trigger onClick event', () => {
      const mockClick = jest.fn()
      const $input = mount(<InputText onClick={mockClick} />)

      $input.find('input').simulate('click')
      expect(mockClick).toHaveBeenCalled()
    })

    it('should trigger onChange event', () => {
      const mockChange = jest.fn()
      const $input = mount(<InputText onChange={mockChange} />)

      $input.find('input').simulate('change')
      expect(mockChange).toHaveBeenCalled()
    })
  })

  describe('onBlur', () => {
    let onBlurSpy

    beforeEach(() => {
      onBlurSpy = jest.fn()
    })

    it('should fire `onBlur` when the input is blurred', () => {
      const $root = shallow(<InputText onBlur={onBlurSpy} />)
      const $inputText = $root.find('input')
      $inputText.simulate('blur')
      expect(onBlurSpy).toHaveBeenCalled()
    })
  })

  describe('onPressEnter', () => {
    let onPressEnterSpy

    beforeEach(() => {
      onPressEnterSpy = jest.fn()
    })

    it('should fire the `onPressEnter` callback when `Enter` is pressed', () => {
      const $root = shallow(<InputText onPressEnter={onPressEnterSpy} />)
      const $inputText = $root.find('input')
      $inputText.simulate('keyPress', { key: 'Enter' })
      expect(onPressEnterSpy).toHaveBeenCalled()
    })

    it('should blur the input by default', () => {
      const $root = shallow(<InputText />)
      const $inputText = $root.find('input')
      $inputText.simulate('keyPress', { key: 'Enter', target: { blur: onPressEnterSpy } })
      expect(onPressEnterSpy).toHaveBeenCalled()
    })

    it('should not fire the event if it is disabled', () => {
      const $root = shallow(<InputText disabled onPressEnter={onPressEnterSpy} />)
      const $input = $root.find('input')
      $input.simulate('keyPress', { key: 'Enter' })
      expect(onPressEnterSpy).not.toHaveBeenCalled()
    })
  })

  describe('disabled', () => {
    it('should render a disabled InputText', () => {
      const $wrap = shallow(<InputText disabled />)
      expect($wrap.find('input')).toHaveLength(1)

      const $input = $wrap.find('input')
      const $root = $wrap.find('.InputText')
      expect($root.hasClass('InputText-disabled')).toEqual(true)
      expect($input.prop('disabled')).toEqual(true)
    })

    it('should not trigger onClick on a disabled InputText', () => {
      const mockClick = jest.fn()
      const $input = mount(<InputText disabled onClick={mockClick} />)

      $input.find('input').simulate('click')
      expect(mockClick).not.toHaveBeenCalled()
    })

    it('should not trigger onChange on a disabled InputText', () => {
      const mockChange = jest.fn()
      const $input = shallow(<InputText disabled onChange={mockChange} />)

      $input.find('input').simulate('change')
      expect(mockChange).not.toHaveBeenCalled()
    })
  })

  describe('Addon', () => {
    it('should have Addon class name', () => {
      const $addon = <Addon><div /></Addon>
      expect(shallow($addon).hasClass('InputTextAddon')).toEqual(true)
    })

    it('should not have Addon by default', () => {
      const $input = shallow(<InputText />)
      expect($input.find(Addon).length).toEqual(0)
    })

    it('should have before addon if specified', () => {
      const $input = shallow(<InputText addonBefore={<div />} />)
      expect($input.find(Addon).length).toEqual(1)
    })

    it('should have after addon if specified', () => {
      const $input = shallow(<InputText addonAfter={<div />} />)
      expect($input.find(Addon).length).toEqual(1)
    })
  })
})
