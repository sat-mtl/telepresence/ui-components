/* global beforeEach jest describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Slider from '~/inputs/Slider/Slider.js'
import Rail from '~/inputs/Slider/Rail.js'

describe('<Slider />', () => {
  let preventDefaultMock
  let onChangeMock

  const fakeEvent = ({ shiftKey } = { shiftKey: false }) => ({
    preventDefault: preventDefaultMock,
    nativeEvent: {
      shiftKey: shiftKey,
      target: document.createElement('div')
    }
  })

  beforeEach(() => {
    preventDefaultMock = jest.fn()
    onChangeMock = jest.fn()
  })

  describe('render', () => {
    it('should render a Slider with a rail and a track by default', () => {
      const $slider = shallow(<Slider />)
      expect($slider.find(Rail)).toHaveLength(1)
    })

    it('should render a Slider with status', () => {
      const $slider = shallow(<Slider status='danger' />)
      expect($slider.hasClass('Slider-danger')).toEqual(true)
    })
  })

  describe('onKeyDown', () => {
    function simulateKeyDown ($slider, event) {
      $slider.simulate('keydown', event)
    }

    it('should call the handler if it is attached to `onKeyDown` property', () => {
      const $slider = shallow(<Slider onChange={onChangeMock} />)
      simulateKeyDown($slider, fakeEvent())
      expect(onChangeMock).toHaveBeenCalled()
    })

    it('should not call the handler if it is disabled', () => {
      const $slider = shallow(<Slider disabled onChange={onChangeMock} />)
      simulateKeyDown($slider, fakeEvent())
      expect(onChangeMock).not.toHaveBeenCalled()
    })
  })

  describe('onWheel', () => {
    function simulateWheelMove ($slider, event) {
      $slider.simulate('wheel', event)
    }

    it('should call the handler if it is attached to `onWheel` property and the shiftKey is pressed', () => {
      const $slider = shallow(<Slider onChange={onChangeMock} />)
      simulateWheelMove($slider, fakeEvent({ shiftKey: true }))
      expect(onChangeMock).toHaveBeenCalled()
    })

    it('should not call the handler if it is disabled', () => {
      const $slider = shallow(<Slider disabled onChange={onChangeMock} />)
      simulateWheelMove($slider)
      expect(onChangeMock).not.toHaveBeenCalled()
    })
  })

  describe('onSlide', () => {
    function simulateMouseSlide ($slider, event) {
      const $rail = $slider.find(Rail).dive()
      $rail.simulate('mousedown')
      $rail.simulate('mouseenter')
      $rail.simulate('mousemove', event)
    }

    it('should call the handler if the mouse is sliding', () => {
      const $slider = shallow(<Slider onChange={onChangeMock} />)
      simulateMouseSlide($slider, fakeEvent())
      expect(onChangeMock).toHaveBeenCalled()
    })

    it('should not call the handler if it is disabled', () => {
      const $slider = shallow(<Slider disabled onChange={onChangeMock} />)
      simulateMouseSlide($slider)
      expect(preventDefaultMock).not.toHaveBeenCalled()
      expect(onChangeMock).not.toHaveBeenCalled()
    })
  })
})
