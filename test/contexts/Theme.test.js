/* global jest afterEach describe it expect beforeEach */

import React from 'react'
import { shallow } from 'enzyme'

import ThemeProvider, { defaultTheme, ThemeContext } from '~/contexts/Theme'

describe('<ThemeProvider />', () => {
  afterEach(() => { jest.resetModules() })

  const mountThemeProvider = ({ theme, isDev }) => {
    return shallow(<ThemeProvider value={theme} isDev={isDev} />)
  }

  describe('fetchThemeFromURL', () => {
    let fetchThemeFromURL

    beforeEach(() => {
      const $provider = mountThemeProvider({})
      const instance = $provider.instance()
      fetchThemeFromURL = instance.fetchThemeFromURL
    })

    it('should find `theme` value from URL parameters', () => {
      const url = 'theme=test'
      expect(fetchThemeFromURL(url)).toEqual('test')
    })

    it('should not find `theme` if it not in the URL parameters', () => {
      const url = 'lol=test'
      expect(fetchThemeFromURL(url)).toEqual(null)
    })
  })

  describe('rendering', () => {
    it('should provide `defaultTheme` when it is mounted without value', () => {
      const $provider = mountThemeProvider({ isDev: false })
      expect($provider.prop('value')).toEqual(defaultTheme)
    })

    it('should provide `defaultTheme` when the theme is not available', () => {
      const $provider = mountThemeProvider({ theme: 'test', isDev: false })
      expect($provider.prop('value')).toEqual(defaultTheme)
    })

    it('should provide defined theme when it is available', () => {
      const $provider = mountThemeProvider({ theme: 'simon', isDev: false })
      const $context = $provider.find(ThemeContext.Provider)
      expect($context.prop('value')).toEqual('simon')
    })
  })

  describe('NODE_ENV', () => {
    let fetchThemeFromURLSpy

    beforeEach(() => {
      fetchThemeFromURLSpy = jest.spyOn(ThemeProvider.prototype, 'fetchThemeFromURL')
    })

    afterEach(() => {
      fetchThemeFromURLSpy.mockClear()
    })

    it('should not use URL fetcher when NODE_ENV is not `dev`', () => {
      const instance = mountThemeProvider({ isDev: false }).instance()
      expect(instance.fetchThemeFromURL).not.toHaveBeenCalled()
    })

    it('should use URL fetcher when NODE_ENV is `dev`', () => {
      const instance = mountThemeProvider({ isDev: true }).instance()
      expect(instance.fetchThemeFromURL).toHaveBeenCalled()
    })
  })
})
