/* global describe it expect */

import React from 'react'
import NavTab from '~/common/NavTab'
import { mount } from 'enzyme'

describe('<NavTab.Panel />', () => {
  const mountPanel = ({ tab, content }) => {
    const $panel = mount(
      <NavTab.Panel tab={tab}>{content}</NavTab.Panel>
    )

    const $content = $panel.find('.navtab-panel')

    return { $panel, $content }
  }

  describe('rendering', () => {
    it('render NavTab.Tab with required properties', () => {
      const { $panel, $content } = mountPanel({ tab: 'tab', content: 'content' })

      expect($content.text()).toEqual('content')
      expect($panel.prop('tab')).toEqual('tab')
    })
  })
})
