/* global jest describe it expect */

import React from 'react'
import NavTab from '~/common/NavTab'
import { mount } from 'enzyme'

describe('<NavTab.ExtraButton />', () => {
  const mountExtraButton = ({ onClick, style, title }) => {
    const $btn = mount(
      <NavTab.ExtraButton
        onClick={onClick}
        style={style}
      >{title}
      </NavTab.ExtraButton>
    )

    const $tab = $btn.find(NavTab.Tab)

    return { $btn, $tab }
  }

  describe('rendering', () => {
    it('render NavTab.ExtraButton with default properties', () => {
      const { $tab } = mountExtraButton({ title: 'button' })

      expect($tab.length).toEqual(1)
      expect($tab.hasClass('navtab-extra-button')).toEqual(true)
      expect($tab.find('.navtab-tab-title').text()).toEqual('button')
    })

    it('render onClick for NavTab.ExtraButton', () => {
      const onClick = jest.fn()
      const { $tab } = mountExtraButton({
        title: 'button',
        onClick: onClick
      })

      $tab.simulate('click')
      expect(onClick).toHaveBeenCalled()
    })
  })
})
