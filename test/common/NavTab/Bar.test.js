/* global describe it expect */

import React from 'react'
import Bar from '~/common/NavTab/Bar'
import { mount } from 'enzyme'

describe('<NavTab.Bar />', () => {
  const mountBar = ({ el, before, after }) => {
    const $bar = mount(<Bar addonBefore={before} addonAfter={after}>{el}</Bar>)

    const $before = $bar.find('.navtab-bar-addon-before')
    const $after = $bar.find('.navtab-bar-addon-after')

    return { $bar, $before, $after }
  }

  describe('rendering', () => {
    it('render NavTab.Bar with default properties', () => {
      const { $bar, $before, $after } = mountBar({ el: <span className='test' /> })

      expect($bar.find('ul').length).toEqual(1)
      expect($bar.find('.test').length).toEqual(1)

      expect($before.length).toEqual(0)
      expect($after.length).toEqual(0)
    })

    it('render NavTab with addons', () => {
      const { $before, $after } = mountBar({
        el: <span className='test' />,
        before: <span className='before-test' />,
        after: <span className='after-test' />
      })

      expect($before.length).toEqual(1)
      expect($after.length).toEqual(1)
    })
  })
})
