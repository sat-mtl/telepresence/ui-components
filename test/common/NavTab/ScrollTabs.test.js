/* global jest describe it expect beforeEach afterEach */

import React from 'react'
import NavTab from '~/common/NavTab'
import { shallow } from 'enzyme'

describe('<NavTab.ScrollTabs />', () => {
  const shallowScrollTabs = ({ onOverflowChange, onScrollChange, scrollLeft }, disableLifecycleMethods = false) => {
    return shallow(
      <NavTab.ScrollTabs onOverflowChange={onOverflowChange} onScrollChange={onScrollChange} scrollLeft={scrollLeft}>
        <NavTab.Tab>Test1</NavTab.Tab>
      </NavTab.ScrollTabs>,
      { disableLifecycleMethods: disableLifecycleMethods }
    )
  }

  describe('lifecycle', () => {
    let updateOverflowSpy, updateScrollLeftSpy

    beforeEach(() => {
      updateOverflowSpy = jest.spyOn(NavTab.ScrollTabs.prototype, 'updateOverflow')
      updateScrollLeftSpy = jest.spyOn(NavTab.ScrollTabs.prototype, 'updateScrollLeft')
    })

    afterEach(() => {
      updateOverflowSpy.mockClear()
      updateScrollLeftSpy.mockClear()
    })

    describe('componentDidMount', () => {
      it('should update scrollLeft and overflow', () => {
        shallowScrollTabs({})

        expect(updateOverflowSpy).toHaveBeenCalledTimes(1)
        expect(updateScrollLeftSpy).toHaveBeenCalledTimes(1)
      })
    })

    describe('componentDidMount and componentDidUpdate', () => {
      it('should update scrollLeft and overflow', () => {
        shallowScrollTabs({}).setState({ scrollLeft: 1 })

        expect(updateOverflowSpy).toHaveBeenCalledTimes(2)
        expect(updateScrollLeftSpy).toHaveBeenCalledTimes(2)
      })
    })
  })

  describe('updates', () => {
    let $bar = {}

    beforeEach(() => {
      $bar = {
        current: {
          scrollLeft: 0,
          offsetWidth: 0,
          scrollWidth: 0
        }
      }
    })

    const shallowScrollTabsInstance = ({ onOverflowChange, onScrollChange, scrollLeft }) => {
      const $tabs = shallowScrollTabs({ onOverflowChange, onScrollChange, scrollLeft }, false)
      return $tabs.instance()
    }

    describe('updateOverflow', () => {
      it('should not call `onOverflowChange` if there is no overflow', () => {
        const onOverflowChange = jest.fn()
        const tabs = shallowScrollTabsInstance({ onOverflowChange: onOverflowChange })

        tabs.$bar = $bar
        tabs.updateOverflow()

        expect(onOverflowChange).not.toHaveBeenCalled()
      })

      it('should call `onOverflowChange` if there is an overflow', () => {
        const onOverflowChange = jest.fn()
        const tabs = shallowScrollTabsInstance({ onOverflowChange: onOverflowChange })

        $bar.current.scrollWidth = 1
        tabs.$bar = $bar
        tabs.updateOverflow()

        expect(onOverflowChange).toHaveBeenCalledWith(true, $bar.current)
      })
    })

    describe('updateScrollLeft', () => {
      it('should not call `updateScrollLeft` if scrollLeft === 0', () => {
        const onScrollChange = jest.fn()
        const tabs = shallowScrollTabsInstance({ onScrollChange: onScrollChange })

        tabs.$bar = $bar
        tabs.updateScrollLeft()

        expect(onScrollChange).not.toHaveBeenCalled()
      })

      it('should call `updateScrollLeft` if scrollLeft > 0', () => {
        const scrollLeft = 1
        const onScrollChange = jest.fn()
        const tabs = shallowScrollTabsInstance({ onScrollChange: onScrollChange, scrollLeft: scrollLeft })

        tabs.$bar = $bar
        tabs.updateScrollLeft()

        expect(onScrollChange).toHaveBeenCalledWith(scrollLeft, $bar.current)
      })
    })
  })
})
