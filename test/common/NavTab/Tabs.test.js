/* global jest describe it expect */

import React from 'react'
import NavTab from '~/common/NavTab'
import Bar from '~/common/NavTab/Bar'
import { mount, shallow } from 'enzyme'

describe('<NavTab.Tabs />', () => {
  const findChildren = ($tabs) => {
    const $panel = $tabs.find(NavTab.Panel)
    const $tab = $tabs.find(NavTab.Tab)
    const $btn = $tabs.find(NavTab.ExtraButton)

    return { $tabs, $tab, $panel, $btn }
  }

  const mountTabs = (children) => {
    const $tabs = mount(<NavTab.Tabs>{children}</NavTab.Tabs>)
    return findChildren($tabs)
  }

  const shallowTabs = (children) => {
    const $tabs = shallow(<NavTab.Tabs>{children}</NavTab.Tabs>)
    return findChildren($tabs)
  }

  describe('rendering', () => {
    it('render NavTab.Tabs with default properties', () => {
      const { $tabs, $tab, $panel, $btn } = mountTabs(<span className='test' />)

      expect($tabs.find(Bar).length).toEqual(1)
      expect($tab.length).toEqual(0)
      expect($panel.length).toEqual(0)
      expect($btn.length).toEqual(0)
    })

    it('render NavTab.Tabs with NavTab.Panels', () => {
      const { $tabs, $tab, $panel, $btn } = mountTabs([
        <NavTab.Panel key='t1' tab='Test1'>Test1</NavTab.Panel>,
        <NavTab.Panel key='t2' tab='Test2'>Test2</NavTab.Panel>
      ])

      expect($tabs.find(Bar).length).toEqual(1)
      expect($tab.length).toEqual(2)
      expect($panel.length).toEqual(0)
      expect($btn.length).toEqual(0)

      $tabs.setState({ show: 'Test1' })
      expect($tabs.find(NavTab.Panel).length).toEqual(1)
    })

    it('render NavTab.Tabs with NavTab.Tab', () => {
      const { $tabs, $tab, $panel, $btn } = mountTabs([
        <NavTab.Tab key='t1'>Test1</NavTab.Tab>,
        <NavTab.Tab key='t2'>Test2</NavTab.Tab>
      ])

      expect($tabs.find(Bar).length).toEqual(1)
      expect($tab.length).toEqual(2)
      expect($panel.length).toEqual(0)
      expect($btn.length).toEqual(0)
    })

    it('render NavTab.Tabs with NavTab.ExtraButton', () => {
      const { $tabs, $tab, $panel, $btn } = mountTabs([
        <NavTab.ExtraButton key='t1'>Test1</NavTab.ExtraButton>,
        <NavTab.ExtraButton key='t2'>Test2</NavTab.ExtraButton>
      ])

      expect($tabs.find(Bar).length).toEqual(1)
      expect($tab.length).toEqual(2)
      expect($panel.length).toEqual(0)
      expect($btn.length).toEqual(2)
    })
  })

  describe('setState', () => {
    it('should change state when tab is clicked from NavTab.Panel', () => {
      const setState = jest.fn()
      const { $tabs, $tab } = shallowTabs([
        <NavTab.Panel key='t1' tab='Test1'>Test1</NavTab.Panel>
      ])

      const tabs = $tabs.instance()
      tabs.setState = setState
      $tab.simulate('click')

      expect(setState).toHaveBeenCalled()
    })

    it('should change state when tab is clicked from NavTab.Panel', () => {
      const setState = jest.fn()
      const onClick = jest.fn()

      const { $tabs, $tab } = shallowTabs([
        <NavTab.Tab key='t1' onClick={onClick}>Test1</NavTab.Tab>
      ])

      const tabs = $tabs.instance()
      tabs.setState = setState
      $tab.simulate('click')

      expect(setState).toHaveBeenCalled()
      expect(onClick).toHaveBeenCalled()
    })
  })

  describe('clearMaps', () => {
    it('should clear all rendered maps', () => {
      const { $tabs } = shallowTabs([
        <NavTab.Panel key='t1' tab='Test1'>Test1</NavTab.Panel>,
        <NavTab.Panel key='t2' tab='Test2'>Test2</NavTab.Panel>
      ])

      const tabs = $tabs.instance()
      expect(tabs.tabMap.size).toEqual(2)
      expect(tabs.contentMap.size).toEqual(2)

      tabs.clearMaps()
      expect(tabs.tabMap.size).toEqual(0)
      expect(tabs.contentMap.size).toEqual(0)
    })
  })
})
