/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import { mockedEvent, mockEventFunction } from '~test/mock-component'
import Menu, { SelectButton } from '~/common/Menu/Menu/Menu.js'
import Wrapper from '~/common/Menu/Wrapper'

describe('<Menu />', () => {
  const emptyString = ''

  beforeEach(() => {
    const { defaultProps } = Menu
    defaultProps.onClick = jest.fn()
    defaultProps.onBlur = jest.fn()

    const onMouseDown = defaultProps.onMouseDown
    defaultProps.onMouseDown = mockEventFunction(onMouseDown)
  })

  const shallowMenu = ({ title, expanded, addonBefore, addonAfter, onClick, dataMenu }) => {
    const $select = mount(
      <Menu
        title={title}
        expanded={expanded}
        addonBefore={addonBefore}
        addonAfter={addonAfter}
        onClick={onClick}
        dataMenu={dataMenu}
      />
    )

    const $root = $select.find(SelectButton)
    const $btn = $root.find('button')
    const $menu = $select.find(Wrapper)

    return { $select, $root, $btn, $menu }
  }

  describe('rendering', () => {
    it('should hide wrapped menu by default', () => {
      const { $btn, $menu } = shallowMenu({})

      expect($btn.text()).toEqual(emptyString)
      expect($menu).toHaveLength(1)
      expect($menu.find('ul').prop('style')).toHaveProperty('display', 'none')
    })

    it('render an addon before the Menu title if it is defined', () => {
      const { $select } = shallowMenu({
        addonBefore: <div className='addon-before' />
      })

      expect($select.find('.addon-before').length).toEqual(1)
    })

    it('render an addon after the Menu title if it is defined', () => {
      const { $select } = shallowMenu({
        addonAfter: <div className='addon-after' />
      })

      expect($select.find('.addon-after').length).toEqual(1)
    })

    it('render Menu.Select with a title', () => {
      const testTitle = 'test'
      const { $btn } = shallowMenu({ title: testTitle })

      expect($btn.text()).toEqual(testTitle)
    })

    it('render Menu.Group with `expandMenu` property', () => {
      const { $menu } = shallowMenu({ expanded: true })

      expect($menu).toHaveLength(1)
      expect($menu.prop('menuRef')).not.toBeDefined()
      expect($menu.prop('top')).toEqual(true)
    })
  })

  describe('events', () => {
    it('should select and simulate a click from the data attributes', () => {
      const onClickSpy = jest.fn()
      const { $select } = shallowMenu({ dataMenu: 'Test', onClick: onClickSpy })
      const $btn = $select.find('[data-menu="Test"]')
      $btn.simulate('click')
      expect(onClickSpy).toHaveBeenCalled()
    })

    it('simulate default `onMouseDown` event onMenu.Select', () => {
      const { $btn } = shallowMenu({})

      $btn.simulate('mousedown')
      expect(mockedEvent.stopPropagation).toHaveBeenCalled()
      expect(mockedEvent.preventDefault).toHaveBeenCalled()
    })
  })
})
