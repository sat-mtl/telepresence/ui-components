/* global describe it jest expect beforeEach afterEach */

import React from 'react'
import { shallow } from 'enzyme'

import Button from '~/common/Button/Button.js'
import withLongMouseDown from '~/common/Button/withLongMouseDown.js'

const MouseDownButton = withLongMouseDown(Button)

describe('withLongMouseDown', () => {
  const CLICK_EVENT = { test: 1 }
  const INTERVAL_DELAY = 100000

  let setIntervalMock, clearIntervalMock

  const shallowButtonWithLongMouseDown = (onClick = undefined) => {
    const $btn = shallow(
      <MouseDownButton
        onClick={onClick}
        intervalDelay={INTERVAL_DELAY}
      />
    )

    return [$btn, $btn.instance()]
  }

  beforeEach(() => {
    jest.useFakeTimers()

    setIntervalMock = jest.spyOn(global, 'setInterval')
    clearIntervalMock = jest.spyOn(global, 'clearInterval')
  })

  describe('render', () => {
    it('should render a Button with added capabilities', () => {
      const [$btn] = shallowButtonWithLongMouseDown()

      expect($btn).toBeDefined()
      expect($btn.is(Button)).toEqual(true)
    })
  })

  describe('startIntervalTimer', () => {
    afterEach(() => {
      jest.clearAllTimers()
    })

    it('should start interval with a defined intervalDelay', () => {
      const [, btn] = shallowButtonWithLongMouseDown()

      btn.startIntervalTimer()

      expect(setIntervalMock).toHaveBeenCalledTimes(1)
      expect(setIntervalMock).toHaveBeenCalledWith(expect.any(Function), INTERVAL_DELAY)
    })

    it('should not start interval if it was already started', () => {
      const [, btn] = shallowButtonWithLongMouseDown()

      btn.interval = 1
      btn.startIntervalTimer()

      expect(setIntervalMock).toHaveBeenCalledTimes(0)
    })

    it('should call onClick when the interval is started', () => {
      const onClickMock = jest.fn()
      const [, btn] = shallowButtonWithLongMouseDown(onClickMock)

      btn.startIntervalTimer(CLICK_EVENT)

      expect(onClickMock).not.toHaveBeenCalled()

      jest.runOnlyPendingTimers()

      expect(onClickMock).toHaveBeenCalledWith(CLICK_EVENT)
    })
  })

  describe('stopIntervalTimer', () => {
    it('should stop interval if it was started', () => {
      const [, btn] = shallowButtonWithLongMouseDown()

      btn.interval = 1
      btn.stopIntervalTimer()

      expect(clearIntervalMock).toHaveBeenCalledTimes(1)
      expect(btn.interval).toEqual(0)
    })

    it('should not stop interval if it was not started', () => {
      const [, btn] = shallowButtonWithLongMouseDown()

      btn.stopIntervalTimer()

      expect(btn.interval).toEqual(0)
      expect(clearIntervalMock).toHaveBeenCalledTimes(0)
    })
  })

  describe('handleLongPress', () => {
    let handleLongPressSpy
    let startIntervalTimerSpy
    let stopIntervalTimerSpy

    beforeEach(() => {
      handleLongPressSpy = jest.spyOn(MouseDownButton.prototype, 'handleLongPress')
      startIntervalTimerSpy = jest.spyOn(MouseDownButton.prototype, 'startIntervalTimer')
      stopIntervalTimerSpy = jest.spyOn(MouseDownButton.prototype, 'stopIntervalTimer')
    })

    it('should not call interval if `onClick` was not defined', () => {
      const [$btn] = shallowButtonWithLongMouseDown()

      $btn.simulate('mousedown')
      $btn.simulate('mouseenter')
      $btn.simulate('mouseup')
      $btn.simulate('mouseleave')

      expect(handleLongPressSpy).toHaveBeenCalledTimes(4)
      expect(startIntervalTimerSpy).toHaveBeenCalledTimes(0)
      expect(stopIntervalTimerSpy).toHaveBeenCalledTimes(0)
    })

    it('should start interval when `mousedown` is triggered', () => {
      const [$btn] = shallowButtonWithLongMouseDown(() => {})
      const MOUSE_EVENT = { ...CLICK_EVENT, type: 'mousedown' }

      $btn.simulate('mousedown', MOUSE_EVENT)

      expect(handleLongPressSpy).toHaveBeenCalledWith(MOUSE_EVENT)
      expect(startIntervalTimerSpy).toHaveBeenCalledWith(MOUSE_EVENT)
      expect(stopIntervalTimerSpy).toHaveBeenCalledTimes(0)
    })

    it('should stop interval when `mouseenter` is triggered', () => {
      const [$btn] = shallowButtonWithLongMouseDown(() => {})
      const MOUSE_EVENT = { ...CLICK_EVENT, type: 'mouseenter' }

      $btn.simulate('mouseenter', MOUSE_EVENT)

      expect(handleLongPressSpy).toHaveBeenCalledWith(MOUSE_EVENT)
      expect(startIntervalTimerSpy).toHaveBeenCalledTimes(0)
      expect(stopIntervalTimerSpy).toHaveBeenCalledTimes(1)
    })

    it('should stop interval when `mouseup` is triggered', () => {
      const [$btn] = shallowButtonWithLongMouseDown(() => {})
      const MOUSE_EVENT = { ...CLICK_EVENT, type: 'mouseup' }

      $btn.simulate('mouseup', MOUSE_EVENT)

      expect(handleLongPressSpy).toHaveBeenCalledWith(MOUSE_EVENT)
      expect(startIntervalTimerSpy).toHaveBeenCalledTimes(0)
      expect(stopIntervalTimerSpy).toHaveBeenCalledTimes(1)
    })

    it('should stop interval when `mouseleave` is triggered', () => {
      const [$btn] = shallowButtonWithLongMouseDown(() => {})
      const MOUSE_EVENT = { ...CLICK_EVENT, type: 'mouseleave' }

      $btn.simulate('mouseleave', MOUSE_EVENT)

      expect(handleLongPressSpy).toHaveBeenCalledWith(MOUSE_EVENT)
      expect(startIntervalTimerSpy).toHaveBeenCalledTimes(0)
      expect(stopIntervalTimerSpy).toHaveBeenCalledTimes(1)
    })
  })
})
