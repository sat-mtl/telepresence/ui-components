/* global describe it expect */

import React from 'react'
import { shallow, mount } from 'enzyme'

import MDSpinner from 'react-md-spinner'

import Spinner from '~/common/Spinner'

describe('<Spinner />', () => {
  it('should mount Spinner with default properties', () => {
    const $spinner = mount(<Spinner />)

    expect($spinner.prop('size')).toEqual(Spinner.defaultProps.size)
    expect($spinner.prop('color')).toEqual(Spinner.defaultProps.color)
    expect($spinner.prop('backdropPos')).toEqual(Spinner.defaultProps.backdropPos)

    const $backdrop = $spinner.find('.spinner-backdrop')
    expect($backdrop.prop('style')).toHaveProperty('position', Spinner.defaultProps.backdropPos)
  })

  it('should mount Spinner with custom properties', () => {
    const testSize = 100
    const testColor = 'testColor'
    const testBackdropPos = 'relative'

    const $spinner = mount(
      <Spinner
        size={testSize}
        color={testColor}
        backdropPos={testBackdropPos}
      />
    )

    expect($spinner.prop('size')).toEqual(testSize)
    expect($spinner.prop('color')).toEqual(testColor)
    expect($spinner.prop('backdropPos')).toEqual(testBackdropPos)

    const $backdrop = $spinner.find('.spinner-backdrop')
    expect($backdrop.prop('style')).toHaveProperty('position', testBackdropPos)
  })

  it('render a MDSpinner without message', () => {
    const $spinner = shallow(<Spinner />)

    expect($spinner.find(MDSpinner)).toHaveLength(1)
    expect($spinner.find('.spinner-message')).toHaveLength(0)
  })

  it('render a message when it is provided', () => {
    const $spinner = shallow(<Spinner message='test' />)

    expect($spinner.find('.spinner-message')).toHaveLength(1)
  })
})
