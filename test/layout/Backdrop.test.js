/* global jest describe it expect */

import React from 'react'
import { shallow } from 'enzyme'
import Backdrop from '~/layout/Backdrop'

describe('<Backdrop />', () => {
  const shallowBackdrop = ({ onClick, zIndex } = {}) => {
    return shallow(
      <Backdrop onClick={onClick} zIndex={zIndex} />
    )
  }

  describe('render', () => {
    it('should render a backdrop', () => {
      expect(shallowBackdrop().hasClass('backdrop')).toEqual(true)
    })

    it('should trigger onClick when the user clicks', () => {
      const onClick = jest.fn()
      const $back = shallowBackdrop({ onClick })
      $back.simulate('click')
      expect(onClick).toHaveBeenCalled()
    })
  })
})
