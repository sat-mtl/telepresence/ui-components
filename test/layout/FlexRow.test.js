/* global describe it expect */

import React from 'react'
import { shallow, mount } from 'enzyme'

import FlexRow from '~/layout/FlexRow'
import FlexBox from '~/layout/FlexBox'

describe('<FlexRow />', () => {
  describe('rendering', () => {
    it('should render a FlexRow from a Flexbox with a row direction', () => {
      const $flex = shallow(<FlexRow />)
      expect($flex).toBeDefined()

      const $box = $flex.find(FlexBox)
      expect($box.length).toEqual(1)
      expect($box.prop('flexDirection')).toEqual('row')
    })

    it('should be rendered by default without wrap', () => {
      const $flex = mount(<FlexRow />)
      const $box = $flex.find(FlexBox)
      expect($box.prop('flexWrap')).toEqual('nowrap')
    })

    it('should be rendered with wrap if flexWrap is set', () => {
      const $flex = mount(<FlexRow flexWrap='wrap' />)
      const $box = $flex.find(FlexBox)
      expect($box.prop('flexWrap')).toEqual('wrap')
    })
  })
})
