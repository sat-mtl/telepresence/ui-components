/* global describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import FlexBox, { renderFlexWrap } from '~/layout/FlexBox.js'

describe('<FlexBox />', () => {
  describe('rendering', () => {
    it('should render a FlexBox', () => {
      const $flex = shallow(<FlexBox />)
      expect($flex).toBeDefined()
      expect($flex.length).toEqual(1)
    })
  })

  describe('renderFlexWrap', () => {
    it('should render flex-wrap as it is defined as a string', () => {
      expect(renderFlexWrap({ flexWrap: 'anything' })).toEqual('anything')
    })

    it('should render flex-wrap as `wrap` if it is defined as `true`', () => {
      expect(renderFlexWrap({ flexWrap: true })).toEqual('wrap')
    })

    it('should render flex-wrap as `nowrap` if it is defined as `false`', () => {
      expect(renderFlexWrap({ flexWrap: false })).toEqual('nowrap')
    })
  })
})
