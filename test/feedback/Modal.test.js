/* global describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Modal from '~/feedback/Modal/Modal'

describe('<Modal />', () => {
  const shallowModal = ({ visible, footer, header, content } = { visible: true }) => {
    return shallow(
      <Modal
        visible={visible}
        footer={footer}
        header={header}
      >
        {content}
      </Modal>
    )
  }

  describe('constructor', () => {
    it('should render a Modal', () => {
      const $modal = shallowModal()
      expect($modal).toBeDefined()
    })

    it('should not render a Modal if it is not visible', () => {
      const $modal = shallowModal({ visible: false })
      expect($modal).toBeDefined()
    })

    it('should not render footer if it is not specified', () => {
      const $modal = shallowModal()
      const $footer = $modal.find('footer')
      expect($footer.length).toEqual(0)
    })

    it('should render footer if it is specified', () => {
      const $modal = shallowModal({ visible: true, footer: 'footer' })
      const $footer = $modal.find('footer')
      expect($footer.length).toEqual(1)
    })

    it('should not render header if it is not specified', () => {
      const $modal = shallowModal()
      const $header = $modal.find('header')
      expect($header.length).toEqual(0)
    })

    it('should render header if it is specified', () => {
      const $modal = shallowModal({ visible: true, header: 'header' })
      const $header = $modal.find('header')
      expect($header.length).toEqual(1)
    })

    it('should render content every time', () => {
      const $modal = shallowModal({ visible: true })
      const $header = $modal.find('main')
      expect($header.length).toEqual(1)
    })
  })
})
