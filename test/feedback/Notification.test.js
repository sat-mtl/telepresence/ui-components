/* global jest beforeEach describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Notification from '~/feedback/Notification'

describe('<Notification />', () => {
  let onCloseSpy, onClickSpy

  beforeEach(() => {
    onCloseSpy = jest.fn()
    onClickSpy = jest.fn()
  })

  const shallowNotification = (props = {}) => {
    return shallow(
      <Notification {...props} />
    )
  }

  describe('constructor', () => {
    it('should render a notification', () => {
      expect(shallowNotification()).toBeDefined()
    })

    it('shouldn\'t add the close button by default', () => {
      const $notification = shallowNotification()
      expect($notification.find('.close').length).toEqual(0)
    })

    it('should add the close button with the `onClose` property', () => {
      const $notification = shallowNotification({ onClose: onCloseSpy })
      expect($notification.find('.close').length).toEqual(1)
    })

    it('should add a onClick handler on the close button with the `onClose` property', () => {
      const $notification = shallowNotification({ onClose: onCloseSpy })
      const $close = $notification.find('.close')
      $close.simulate('click')
      expect(onCloseSpy).toHaveBeenCalled()
    })

    it('should add a onClick handler on the notification with the `onClick` property', () => {
      const $notification = shallowNotification({ onClick: onClickSpy })
      $notification.simulate('click')
      expect(onClickSpy).toHaveBeenCalled()
    })
  })

  describe('description', () => {
    it('should display a description if it is not empty', () => {
      expect(shallowNotification({ description: 'test' }).find('.description').length).toEqual(1)
    })

    it('should not display a description if it is empty', () => {
      expect(shallowNotification({ description: '' }).find('.description').length).toEqual(0)
    })

    it('should not display a description if it is undefined', () => {
      expect(shallowNotification().find('.description').length).toEqual(0)
    })
  })
})
