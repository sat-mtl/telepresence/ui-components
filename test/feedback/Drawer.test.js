/* global jest describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Backdrop from '~/layout/Backdrop'
import Drawer from '~/feedback/Drawer/Drawer'

describe('<Drawer />', () => {
  const shallowDrawer = ({ visible, body, header, footer, onBackdropClick } = { visible: true }) => {
    return shallow(
      <Drawer
        header={header}
        onBackdropClick={onBackdropClick}
        visible={visible}
        footer={footer}
      >
        {body}
      </Drawer>
    )
  }

  describe('constructor', () => {
    it('should render a Drawer', () => {
      const $drawer = shallowDrawer({}).dive()
      expect($drawer).toBeDefined()
      expect($drawer.find(Backdrop).length).toEqual(1)
    })

    it('should not render header when it is not specified', () => {
      const $header = shallowDrawer().find('header')
      expect($header.length).toEqual(0)
    })

    it('should render header when it is specified', () => {
      const $drawer = shallowDrawer({ visible: true, header: 'header' })
      const $header = $drawer.find('header')
      expect($header.length).toEqual(1)
    })

    it('should render the main content every time', () => {
      const $drawer = shallowDrawer({ visible: true })
      const $main = $drawer.find('main')
      expect($main.length).toEqual(1)
    })

    it('should not render footer when it is not specified', () => {
      const $footer = shallowDrawer().find('footer')
      expect($footer.length).toEqual(0)
    })

    it('should render header when it is specified', () => {
      const $drawer = shallowDrawer({ visible: true, footer: 'footer' })
      const $footer = $drawer.find('footer')
      expect($footer.length).toEqual(1)
    })
  })

  describe('<Backdrop />', () => {
    it('should trigger onBackdropClick when it is clicked', () => {
      const mockBackdropClick = jest.fn()
      const $drawer = shallowDrawer({ onBackdropClick: mockBackdropClick })
      const $backdrop = $drawer.find(Backdrop).dive()
      $backdrop.simulate('click')
      expect(mockBackdropClick).toHaveBeenCalled()
    })
  })
})
