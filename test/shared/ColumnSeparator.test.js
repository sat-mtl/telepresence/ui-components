/* global describe it expect */

import React from 'react'
import { shallow } from 'enzyme'

import ColumnSeparator from '~/shared/ColumnSeparator'

describe('<ColumnSeparator />', () => {
  describe('rendering', () => {
    it('should render a ColumnSeparator', () => {
      const $column = shallow(<ColumnSeparator />)
      expect($column).toBeDefined()
      expect($column.length).toEqual(1)
    })
  })
})
