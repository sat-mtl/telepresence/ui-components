/* global expect describe it */

import { normalize, denormalize, convertToPercentage, getDigits, parseDouble, validateNumber } from '~/utils/numberTools'

describe('numberTools', () => {
  describe('normalize', () => {
    it('should fail if min and max are equals', () => {
      expect(() => normalize(0, 0, 0)).toThrow()
    })

    it('normalize given value', () => {
      expect(normalize(0, 100, 50)).toEqual(0.5)
    })
  })

  describe('denormalize', () => {
    it('should fail if the normalize value is upper than 1', () => {
      expect(() => denormalize(0, 100, 2)).toThrow()
    })

    it('should fail if the normalize value is lower than 0', () => {
      expect(() => denormalize(0, 100, -1)).toThrow()
    })

    it('denormalize given value', () => {
      expect(denormalize(0, 100, 0.5)).toEqual(50)
    })
  })

  describe('convertToPercentage', () => {
    it('should return pourcent of any value', () => {
      expect(convertToPercentage(0, 100, 50)).toEqual(50)
    })
  })

  describe('getDigits', () => {
    it('should get digits from any steps', () => {
      expect(getDigits(0.001)).toEqual(3)
    })

    it('should return 0 if the step is upper then 1', () => {
      expect(getDigits(1)).toEqual(0)
    })
  })

  describe('parseDouble', () => {
    it('should fix the float digits', () => {
      expect(parseDouble(3.333333333333333, 2)).toEqual(3.33)
    })

    it('should parse the float as an integer if its digits is 0', () => {
      expect(parseDouble(3.33333333333333, 0)).toEqual(3)
    })
  })

  describe('validateNumber', () => {
    it('should return the maximum if the value is upper than the maximum', () => {
      expect(validateNumber(0, 100, 50, 101)).toEqual(100)
    })

    it('should return the minimum if the value is lower than the minimum', () => {
      expect(validateNumber(0, 100, 50, -1)).toEqual(0)
    })

    it('should return the old value if the value isn\'t a number', () => {
      expect(validateNumber(0, 100, 50, NaN)).toEqual(50)
    })
  })
})
