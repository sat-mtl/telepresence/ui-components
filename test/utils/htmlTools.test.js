/* global expect describe it */

import { getClosestElement, getOffsetValue, getKeyDownValue, getWheelValue } from '~/utils/htmlTools'

describe('htmlTools', () => {
  describe('getClosestElement', () => {
    function fakeElement (className) {
      const $elem = document.createElement('div')
      $elem.classList.add(className)

      return $elem
    }

    it('should return the element itself if it should be selected', () => {
      const $element = fakeElement('test')
      expect(getClosestElement($element, '.test')).toEqual($element)
    })

    it('should return null if the selector can\'t be applied', () => {
      expect(getClosestElement()).toEqual(null)
    })

    it('should return the parent if it should be selected', () => {
      const $element = fakeElement('test')
      const $parent = fakeElement('parent')
      $parent.append($element)
      expect(getClosestElement($element, '.parent')).toEqual($parent)
    })
  })

  describe('getSliderOffsetValue', () => {
    it('should get the offset value', () => {
      expect(getOffsetValue(50, 2, 1, 0, 100)).toEqual(4)
    })

    it('should get the default value if the offset can\'t be computed', () => {
      expect(getOffsetValue(0, 2, 1, 0, 0)).toEqual(1)
    })
  })

  describe('getSliderWheelValue', () => {
    it('should decrement the current value if the delta is negative', () => {
      expect(getWheelValue(1, 1, 1)).toEqual(0)
    })

    it('should increment the current value if the delta is positive', () => {
      expect(getWheelValue(-1, 1, 1)).toEqual(2)
    })

    it('should return the current value if the delta isn\'t a number', () => {
      expect(getWheelValue(NaN, 1, 1)).toEqual(1)
    })
  })

  describe('getSliderKeyDownValue', () => {
    it('should decrement the current value if the arrow left is pressed', () => {
      expect(getKeyDownValue('ArrowLeft', 2, 1)).toEqual(1)
    })

    it('should decrement the current value if the arrow down is pressed', () => {
      expect(getKeyDownValue('ArrowDown', 2, 1)).toEqual(1)
    })

    it('should increment the current value if the arrow up is pressed', () => {
      expect(getKeyDownValue('ArrowUp', 2, 1)).toEqual(3)
    })

    it('should increment the current value if the arrow right is pressed', () => {
      expect(getKeyDownValue('ArrowRight', 2, 1)).toEqual(3)
    })

    it('should return the current value if the key doesn\'t match', () => {
      expect(getKeyDownValue('test', 2, 1)).toEqual(2)
    })
  })
})
