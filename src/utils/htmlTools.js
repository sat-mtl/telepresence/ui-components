import { normalize, denormalize } from './numberTools'

/**
 * Gets the closest element of a selector
 * @param {HTMLElement} $element - The element from which the selector is applied
 * @param {string} selector - A CSS selector
 * @see [The closest documentation]{@link https://developer.mozilla.org/en-US/docs/Web/API/Element/closest}
 * @returns {?HTMLElement} The closest element according to the selector
 */
export function getClosestElement ($element, selector) {
  let $close = null

  if ($element && typeof $element.closest === 'function') {
    $close = $element.closest(selector)
  }

  return $close
}

/**
 * Gets a new value from the offset of a `onMouseMove` event
 * @private
 * @param {number} offsetWidth - Width value of the targeted element
 * @param {number} offsetX - Offset value of the slide on the X axis
 * @param {number} currentValue - Current value of the component
 * @param {number} min - Minimal allowed value
 * @param {number} max - Maximal allowed value
 * @returns {number} A new value from a slide
 * @see [The offsetWidth documentation]{@link https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/offsetWidth}
 * @see [The offsetX documentation]{@link https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/offsetX}
 */
export function getOffsetValue (offsetWidth, offsetX, currentValue, min, max) {
  let value = 0

  try {
    const normalized = normalize(0, offsetWidth, offsetX)
    value = denormalize(min, max, normalized)
  } catch (error) {
    value = currentValue
  }

  return value
}

/**
 * Gets a new value from the `onKeyDown` event
 * It parses the keys `ArrowLeft` (37), `ArrowUp` (38), `ArrowRight` (39) and `ArrowDown` (40)
 * @private
 * @param {string} keyCode - Code of the pressed key
 * @param {number} currentValue - Current value of the component
 * @param {number} step - Step value for each slide
 * @returns {number} A new value from a slide
 * @see [The KeyboardEvent.code documentation]{@link https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code}
 */
export function getKeyDownValue (keyCode, currentValue, step) {
  let value = currentValue

  if (['ArrowRight', 'ArrowUp'].includes(keyCode)) {
    value = currentValue + step
  } else if (['ArrowLeft', 'ArrowDown'].includes(keyCode)) {
    value = currentValue - step
  }

  return value
}

/**
 * Gets a new value from the `onWheel` event
 * @private
 * @param {number} deltaY - Delta number of the wheel move on the Y axis
 * @param {number} currentValue - Current value of the component
 * @param {number} step - Step value for each slide
 * @returns {number} A new value from a slide
 */
export function getWheelValue (deltaY, currentValue, step) {
  let value = currentValue

  if (typeof deltaY === 'number' && isFinite(deltaY)) {
    if (deltaY < 0) {
      value = currentValue + step
    } else {
      value = currentValue - step
    }
  }

  return value
}
