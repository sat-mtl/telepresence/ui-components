import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { createUseStyles } from 'react-jss'

export const renderFlexWrap = props => {
  if (typeof props.flexWrap === 'string') {
    return `${props.flexWrap}`
  } else {
    return props.flexWrap ? 'wrap' : 'nowrap'
  }
}

/**
 * Hook used to set parametrized styles
 * @see {@link https://cssinjs.org JSS library}
 */
const useStyles = createUseStyles({
  flexBox: {
    display: 'flex',

    'flex-direction': props => `${props.flexDirection}`,
    'flex-wrap': renderFlexWrap,
    'justify-content': props => `${props.justifyContent}`,

    'align-items': props => `${props.alignItems}`
  },
  flexColumn: {
    'row-gap': props => `${props.rowGap}px`
  },
  flexRow: {
    'column-gap': props => `${props.columnGap}px`
  },
  flexReverseRow: {
    '& > *:not(:last-child)': {
      'margin-left': props => `${props.rowGap}px`
    }
  }
})

/**
 * Standardized CSS flexbox component
 * @see See [Flexbox spec](https://drafts.csswg.org/css-flexbox-1/#flex-direction)
 * @see See [Flexbox guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
 * @see See [CSS Flexible Box Layout documentation](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout)
 */
function FlexBox ({ id, className, children, ...props }) {
  const classes = useStyles(props)
  const cn = classNames(
    'flex-box',
    className,
    classes.flexBox,
    { [classes.flexRow]: props.flexDirection === 'row' },
    { [classes.flexColumn]: props.flexDirection === 'column' },
    { [classes.flexReverseRow]: props.flexDirection === 'row-reverse' }
  )

  return (
    <div id={id} className={cn}>
      {children}
    </div>
  )
}

FlexBox.propTypes = {
  /** Align the items on the cross axis */
  alignItems: PropTypes.oneOf(['stretch', 'flex-start', 'flex-end', 'center']),
  /** Specifies how items are placed in the flex container */
  flexDirection: PropTypes.oneOf(['row', 'row-reverse', 'column', 'column-reverse']),
  /** Specifies whether items are forced into a single line or can be wrapped into multiple lines */
  flexWrap: PropTypes.oneOfType([
    PropTypes.oneOf(['nowrap', 'wrap', 'wrap-reverse']),
    PropTypes.bool
  ]),
  justifyContent: PropTypes.oneOf([
    'center',
    'start',
    'end',
    'flex-start',
    'flex-end',
    'left',
    'right',
    'normal',
    'space-around',
    'space-evenly',
    'space-between',
    'stretch',
    'unsafe center',
    'initial'
  ]),
  /** Gap value between each column (in pixel) */
  columnGap: PropTypes.number,
  /** Gap value between each row (in pixel) */
  rowGap: PropTypes.number,
  /** All items in the flexbox */
  children: PropTypes.any,
  /** Additional class of the FlexBox */
  className: PropTypes.string,
  /** Optional id of the FlexBox */
  id: PropTypes.string
}

FlexBox.defaultProps = {
  id: null,
  className: '',
  alignItems: 'stretch',
  flexDirection: 'row',
  flexWrap: 'nowrap',
  justifyContent: 'normal',
  columnGap: 0,
  rowGap: 0
}

export default FlexBox
