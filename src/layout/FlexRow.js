import React from 'react'
import FlexBox from './FlexBox'

/** Wrapper around row-based CSS flexbox */
const FlexRow = ({ children, ...props }) => (
  <FlexBox flexDirection='row' {...props}>
    {children}
  </FlexBox>
)

export default FlexRow
