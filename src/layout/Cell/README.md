```js
import React, { useState } from 'react';
import ThemeProvider from '~/contexts/Theme'
import Cell from './index';

import FlexRow from '~/layout/FlexRow'
import Icon from '~/common/Icon';

const ConnectionCell = ({ disabled }) => {
  const [isArmed, arm] = useState(false)
  const [isHovered, hover] = useState(false)

  return (
    <Cell
      width={70}
      height={50}
      padding={20}
      disabled={disabled}
      color={isHovered && isArmed ? '#d84b52' : '#262626'}
      onClick={() => { arm(!isArmed) }}
      onMouseEnter={() => { hover(true) }}
      onMouseLeave={() => { hover(false) }}
    >
      {(!isHovered && isArmed) || (isHovered && !isArmed) ? <Icon type='flux' color='#fff' /> : null}
      {isHovered && isArmed ? <Icon type='close' color='#fff' /> : null}
    </Cell>
  )
}

<FlexRow columnGap={10}>
  <Cell color={'aliceblue'} width={40} height={50} />
  <Cell width={70} height={50} padding={10} color='red'>
    <Icon type='audio' />
  </Cell>
  <Cell width={70} height={50} color='red' onClick={() => { console.log('I am a cell') }}>
    <img src='http://placehold.it/160x120&text=image1' />
  </Cell>
  <ConnectionCell />
  <ConnectionCell disabled />
</FlexRow>
```
