import React, { forwardRef } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import { createUseStyles } from 'react-jss'

export const renderColor = props => {
  return props.color ? `${props.color}` : null
}

const useStyles = createUseStyles({
  cell: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: renderColor,

    margin: props => props.margin ? `${props.margin}px` : '0',

    height: props => props.height ? `${props.height}px` : '100%',
    width: props => props.width ? `${props.width}px` : '100%',

    '& > *': {
      height: props => props.height ? `${props.height - props.padding}px` : '100%',
      width: props => props.width ? `${props.width - props.padding}px` : '100%'
    }
  }
})

/**
 * Cell shows content in a table layout
 * It uses the `React.forwardRef` function that adds a `ref` property.
 * This makes possible to inject custom events in the Cell element.
 * @see [Forwarding Refs documentation]{@link https://reactjs.org/docs/forwarding-refs.html}
 */
const Cell = forwardRef(({ children, ...props }, ref) => {
  const { className, disabled, selected, onClick } = props
  const classes = useStyles(props)

  const cn = classNames(
    'Cell',
    className,
    classes.cell,
    { selected: !!selected },
    { disabled: !!disabled }
  )

  const onCellClick = (e) => {
    if (!disabled && onClick) onClick(e)
  }

  return (
    <div className={cn} ref={ref} onClick={onCellClick}>
      {children}
    </div>
  )
})

Cell.propTypes = {
  /** Disables/Enables the cell */
  disabled: PropTypes.bool,
  /** Select the shmdata cell */
  selected: PropTypes.bool,
  /** Adjust the cell width */
  width: PropTypes.number,
  /** Adjust the cell height */
  height: PropTypes.number,
  /** Adjust the cell padding */
  padding: PropTypes.number,
  /** Representative icon of the shmdata */
  children: PropTypes.node,
  /** Callback triggered when the shmdata is clicked */
  onClick: PropTypes.func,
  /** Add an extra class name */
  className: PropTypes.string
}

Cell.defaultProps = {
  classNames: '',
  disabled: false,
  onClick: null,
  padding: 0
}

export default Cell
