/**
 * Enum for all UI statuses
 * @readonly
 * @enum {string}
 */
const StatusEnum = Object.freeze({
  FOCUS: 'focus',
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  DANGER: 'danger',
  BUSY: 'busy'
})

export default StatusEnum
