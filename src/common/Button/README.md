# Button example

```js
import React, { useState } from 'react'

import ThemeProvider from '~/contexts/Theme'

import FlexColumn from '~/layout/FlexColumn'
import FlexRow from '~/layout/FlexRow'
import Icon from '~/common/Icon';

import Button, { types, shapes } from './Button.js';
import './index.js'

function capitalize (title) {
  return title.charAt(0).toUpperCase() + title.slice(1)
};

function craftButton (title, type, shape = 'rectangle', disabled, outlined) {
  const key = type ? type : shape

  return (
    <Button key={key} shape={shape} type={type} disabled={disabled} outlined={outlined}>
      {title}
    </Button>
  )
};

<div style={{ padding: '20px', backgroundColor: '#222' }}>
  <ThemeProvider value='simon'>
    <FlexColumn key='types' rowGap={8}>
      <FlexRow columnGap={8}>
        {types.map(type => {
          return craftButton(capitalize(type.split('-')[0]), type, 'rectangle', false)
        })}
      </FlexRow>
      <FlexRow key='outlined' columnGap={8}>
        {types.slice(0, 3).map(type => {
          return craftButton('Outlined', type, 'rectangle', false, true)
        })}
      </FlexRow>
      <FlexRow key='withIcon' columnGap={8}>
        {types.map(type => {
          return craftButton(<Icon type='audio' />, type, 'circle')
        })}
        {types.slice(0, 3).map(type => {
          return craftButton(<Icon type='audio' />, type, 'circle', false, true)
        })}
      </FlexRow>
      <FlexRow key='squares' columnGap={8}>
        {types.map(type => {
          return craftButton(<Icon type='settings' />, type, 'square')
        })}
        {types.slice(0, 3).map(type => {
          return craftButton(<Icon type='settings' />, type, 'square', false, true)
        })}
      </FlexRow>
      <FlexRow key='dummy' columnGap={8}>
        {shapes.map(shape => {
          return craftButton('NO', undefined, shape, true)
        })}
      </FlexRow>
    </FlexColumn>
  </ThemeProvider>
</div>
```

You can add a long mouse pressed behaviour on this button by using the HOC component `withLongMouseDown`. Be careful, the `setInterval` update from the long press behaviour cannot be used with React hooks into the same component. You should use a controller.

```js
import React, { Component } from 'react'
import Button from './Button.js';
import FlexRow from '~/layout/FlexRow'
import withLongMouseDown from './withLongMouseDown.js';

const ButtonWithLongPress = withLongMouseDown(Button)

class Controller {
  constructor () {
    this.listeners = []
  }

  updateListeners () {
    this.listeners.forEach(f => { f() })
  }

  addListener (func) {
    this.listeners.push(func)
  }
}

const ctrl = new Controller();

class ButtonText extends Component {
  constructor(props) {
    super(props)
    this.state = { value: 0 }
  }

  componentDidMount () {
    this.props.ctrl.addListener(() => {
      this.setState({ value: this.state.value + 1 })
    })
  }

  render() {
    return (
      <div>
        Value: {this.state.value}
      </div>
    )
  }
}

<FlexRow columnGap={50}>
  <ButtonText ctrl={ctrl} />
  <ButtonWithLongPress onClick={() => { ctrl.updateListeners() }}>
    Press Me
  </ButtonWithLongPress>
</FlexRow>
```

## WIP: Gabrielle style

```js
import React, { useState } from 'react'

import ThemeProvider from '~/contexts/Theme'

import FlexColumn from '~/layout/FlexColumn'
import FlexRow from '~/layout/FlexRow'
import Icon from '~/common/Icon';

import Button, { sizes, variants } from './Button.js';
import './index.js'

const types = ['primary', 'secondary', 'disabled']

function capitalize (title) {
  return title.charAt(0).toUpperCase() + title.slice(1)
};

function craftButton (title, type, disabled, size, variant) {
  const key = type ? type : shape

  return (
    <Button key={key} type={type} disabled={type === 'disabled'} size={size} variant={variant}>
      {title}
    </Button>
  )
};

<div style={{ padding: '20px', backgroundColor: '#222' }}>
  <ThemeProvider value='gabrielle'>
    <FlexColumn key='types' rowGap={16}>
        {sizes.map(size => (
          <FlexColumn rowGap={8} key={size}>
            <FlexRow columnGap={8}>
              {types.map(type => {
                return craftButton(capitalize(type), type, false, size, 'contained')
            })}
          </FlexRow>
          <FlexRow columnGap={8}>
            {types.map(type => {
              return craftButton(capitalize(type), type, false, size, 'outlined')
            })}
          </FlexRow>
          <FlexRow columnGap={8}>
            {types.map(type => {
              return craftButton(capitalize(type), type, false, size, 'text')
            })}
          </FlexRow>
        </FlexColumn>
        ))}
    </FlexColumn>
  </ThemeProvider>
</div>
```
