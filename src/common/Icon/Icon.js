import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { createUseStyles } from 'react-jss'

import { ThemeContext } from '~/contexts/Theme'

// Scenic icons
import AddContact from '@assets/icons/scenic/add-contact.svg'
import Audio from '@assets/icons/scenic/audio.svg'
import Call from '@assets/icons/scenic/call.svg'
import Cast from '@assets/icons/scenic/cast.svg'
import ChevronDown from '@assets/icons/scenic/chevron-down.svg'
import ChevronLeft from '@assets/icons/scenic/chevron-left.svg'
import ChevronRight from '@assets/icons/scenic/chevron-right.svg'
import ChevronUp from '@assets/icons/scenic/chevron-up.svg'
import Close from '@assets/icons/scenic/close.svg'
import Configure from '@assets/icons/scenic/configure.svg'
import Contacts from '@assets/icons/scenic/contacts.svg'
import Data from '@assets/icons/scenic/data.svg'
import Delete from '@assets/icons/scenic/delete.svg'
import Download from '@assets/icons/scenic/download.svg'
import Eye from '@assets/icons/scenic/eye.svg'
import EyeBlind from '@assets/icons/scenic/eye-blind.svg'
import Flux from '@assets/icons/scenic/flux.svg'
import Hangup from '@assets/icons/scenic/hangup.svg'
import Info from '@assets/icons/scenic/info.svg'
import Lock from "@assets/icons/scenic/lock.svg"
import Matrix from '@assets/icons/scenic/matrice.svg'
import Plus from '@assets/icons/scenic/plus.svg'
import Minus from '@assets/icons/scenic/minus.svg'
import Settings from '@assets/icons/scenic/settings.svg'
import Signal from '@assets/icons/scenic/signal.svg'
import Switcher from '@assets/icons/scenic/switcher.svg'
import Video from '@assets/icons/scenic/video.svg'
import Alert from '@assets/icons/scenic/alert.svg'
import NewFile from '@assets/icons/scenic/new-file.svg'
import SaveFileAs from '@assets/icons/scenic/save-as.svg'
import SaveFile from '@assets/icons/scenic/save.svg'
import OpenFolder from '@assets/icons/scenic/open-folder.svg'

// ScenicLight icons
import Select from '@assets/icons/scenicLight/select.svg'
import NdiAddon from '@assets/icons/scenicLight/stroke.svg'
import BluetoothAddon from '@assets/icons/scenicLight/bluetooth.svg'
import User from '@assets/icons/scenicLight/user.svg'
import Users from '@assets/icons/scenicLight/users.svg'
import Mic from '@assets/icons/scenicLight/mic.svg'
import MicShut from '@assets/icons/scenicLight/mic-off.svg'
import Monitor from '@assets/icons/scenicLight/monitor.svg'
import Headphones from '@assets/icons/scenicLight/headphones.svg'
import VideoCamera from '@assets/icons/scenicLight/video.svg'
import Smartphone from '@assets/icons/scenicLight/smartphone.svg'
import SpeakerLight from '@assets/icons/scenicLight/speaker.svg'
import EyeOpen from '@assets/icons/scenicLight/eye.svg'
import EyeShut from '@assets/icons/scenicLight/eye-off.svg'
import Check from '@assets/icons/scenicLight/check.svg'
import Home from '@assets/icons/scenicLight/home.svg'
import LightBulb from '@assets/icons/scenicLight/lightbulb.svg'
import Events from '@assets/icons/scenicLight/events.svg'
import ContactsList from '@assets/icons/scenicLight/contacts.svg'
import PlayVideo from '@assets/icons/scenicLight/play-video.svg'
import FormEye from '@assets/icons/scenicLight/form-eye.svg'
import SceneVideo from '@assets/icons/scenicLight/video-scene.svg'
import SceneAudio from '@assets/icons/scenicLight/audio-scene.svg'
import SceneVideoActive from '@assets/icons/scenicLight/video-scene-active.svg'
import SceneAudioActive from '@assets/icons/scenicLight/audio-scene-active.svg'
import SceneScreen from '@assets/icons/scenicLight/scene-screen.svg'

// Bibliolab icons
import Camera from '@assets/icons/bibliolab/camera.svg'
import Catchbox from '@assets/icons/bibliolab/catchbox.svg'
import Chairs from '@assets/icons/bibliolab/chairs.svg'
import Conference from '@assets/icons/bibliolab/conference.svg'
import Headset from '@assets/icons/bibliolab/headset.svg'
import Laptop from '@assets/icons/bibliolab/laptop.svg'
import List from '@assets/icons/bibliolab/list.svg'
import Micro from '@assets/icons/bibliolab/mic.svg'
import MicroMute from '@assets/icons/bibliolab/mic-mute.svg'
import Pages from '@assets/icons/bibliolab/pages.svg'
import Pause from '@assets/icons/bibliolab/pause-1.svg'
import Play from '@assets/icons/bibliolab/play-1.svg'
import Projector from '@assets/icons/bibliolab/projector.svg'
import ShapedQuestion from '@assets/icons/bibliolab/question.svg'
import BbSettings from '@assets/icons/bibliolab/settings.svg'
import Speaker from '@assets/icons/bibliolab/speaker.svg'
import SpeakerMute from '@assets/icons/bibliolab/mute.svg'
import Stop from '@assets/icons/bibliolab/stop-1.svg'
import Discussion from '@assets/icons/bibliolab/talk.svg'
import Material from '@assets/icons/bibliolab/material.svg'

import ShapedCritical from '@assets/icons/bibliolab/critical.svg'
import ShapedPause from '@assets/icons/bibliolab/pause-2.svg'
import ShapedPlay from '@assets/icons/bibliolab/play-2.svg'
import ShapedPower from '@assets/icons/bibliolab/power.svg'
import ShapedStop from '@assets/icons/bibliolab/stop-2.svg'
import ShapedWarning from '@assets/icons/bibliolab/warning.svg'

import './Icon.scss'

export const scenicOutlined = {
  'add-contact': <AddContact />,
  call: <Call />,
  cast: <Cast />,
  close: <Close />,
  configure: <Configure />,
  contacts: <Contacts />,
  data: <Data />,
  delete: <Delete />,
  download: <Download />,
  eye: <Eye />,
  'eye-blind': <EyeBlind />,
  flux: <Flux />,
  hangup: <Hangup />,
  info: <Info />,
  lock: <Lock />,
  matrix: <Matrix />,
  plus: <Plus />,
  minus: <Minus />,
  settings: <Settings />,
  signal: <Signal />,
  switcher: <Switcher />,
  alert: <Alert />,
  'new-file': <NewFile />,
  'save-file': <SaveFile />,
  'save-file-as': <SaveFileAs />,
  'open-folder': <OpenFolder />
}

export const bibliolabOutlined = {
  audio: <Audio />,
  video: <Video />,
  camera: <Camera />,
  catchbox: <Catchbox />,
  chairs: <Chairs />,
  conference: <Conference />,
  discussion: <Discussion />,
  headset: <Headset />,
  laptop: <Laptop />,
  list: <List />,
  micro: <Micro />,
  'micro-mute': <MicroMute />,
  material: <Material />,
  pages: <Pages />,
  projector: <Projector />,
  'bb-settings': <BbSettings />,
  speaker: <Speaker />,
  'speaker-mute': <SpeakerMute />
}

export const outlined = {
  ...bibliolabOutlined,
  ...scenicOutlined
}

export const controls = {
  pause: <Pause />,
  play: <Play />,
  stop: <Stop />
}

export const chevrons = {
  up: <ChevronUp />,
  down: <ChevronDown />,
  left: <ChevronLeft />,
  right: <ChevronRight />
}

export const select = {
  dropDown: <Select />
}

export const connectors = {
  ndi: <NdiAddon />,
  bluetooth: <BluetoothAddon />
}

export const devices = {
  monitor: <Monitor />,
  headphones: <Headphones />,
  videoCamera: <VideoCamera />,
  mic: <Mic />,
  micShut: <MicShut />,
  smartphone: <Smartphone />,
  speakerLight: <SpeakerLight />,
  playVideo: <PlayVideo />
}

export const navigation = {
  home: <Home />,
  lightbulb: <LightBulb />,
  contactsList: <ContactsList />,
  events: <Events />
}

export const setup = {
  user: <User />,
  users: <Users />,
  eyeOpen: <EyeOpen />,
  eyeShut: <EyeShut />,
  check: <Check />,
  formEye: <FormEye />,
  sceneVideo: <SceneVideo />,
  sceneAudio: <SceneAudio />,
  sceneVideoActive: <SceneVideoActive />,
  sceneAudioActive: <SceneAudioActive />,
  sceneScreen: <SceneScreen />
}

export const filled = {
  'filled-critical': <ShapedCritical />,
  'filled-pause': <ShapedPause />,
  'filled-play': <ShapedPlay />,
  'filled-power': <ShapedPower />,
  'filled-question': <ShapedQuestion />,
  'filled-stop': <ShapedStop />,
  'filled-warning': <ShapedWarning />
}

const assets = {
  ...outlined,
  ...controls,
  ...filled,
  ...chevrons,
  ...select,
  ...connectors,
  ...devices,
  ...navigation,
  ...setup
}

/**
 * Hook used to set parametrized styles
 * @see {@link https://cssinjs.org JSS library}
 */
const useStyles = createUseStyles({
  icon: {
    display: 'flex',
    'justify-content': 'center',

    '& svg': {
      width: '100%',
      height: '100%'
    },

    color: props => props.color || '#000',

    '& .layer-shape': {
      color: props => props.color || '#000'
    },

    '& .layer-symbol': {
      color: '#fff'
    }
  }
})

function Icon ({ ...props }) {
  const classes = useStyles(props)
  const { type, withTheme } = props
  const theme = useContext(ThemeContext)

  const cn = classNames(
    `icon-${type}`,
    `icon-${type}-${theme}`,
    { 'icon-with-theme': withTheme },
    classes.icon
  )

  return (
    <div className={cn}>
      {assets[type]}
    </div>
  )
}

Icon.propTypes = {
  type: PropTypes.oneOf(Object.keys(assets)).isRequired,
  color: PropTypes.string,
  withTheme: PropTypes.bool.isRequired
}

Icon.defaultProps = {
  withTheme: false
}

Icon.outlined = outlined
Icon.controls = controls
Icon.filled = filled

export default Icon
