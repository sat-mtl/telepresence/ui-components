import Button from './Button'
import Icon from './Icon'
import Menu from './Menu'
import NavTab from './NavTab'
import Spinner from './Spinner'
import StatusEnum from './StatusEnum'
import Tag from './Tag'

export default {
  Button,
  Icon,
  Menu,
  NavTab,
  Spinner,
  StatusEnum,
  Tag
}
