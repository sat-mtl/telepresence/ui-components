import React from 'react'

import MDSpinner from 'react-md-spinner'
import PropTypes from 'prop-types'

import './style.legacy.scss'

function renderMessage (message) {
  if (!message) return null

  return (
    <span className='spinner-message'>
      {message}
    </span>
  )
}

/**
 * A spinner for all loading states in the Scenic application
 */
export default function Spinner ({ size, color, message, backdropPos }) {
  return (
    <div className='spinner-backdrop' style={{ position: backdropPos }}>
      <span className='spinner-container'>
        <MDSpinner size={size} singleColor={color} />
        {renderMessage(message)}
      </span>
    </div>
  )
}

Spinner.propTypes = {
  /**
   * The size of the spinner
   * @see [MDSpinner properties](https://github.com/tsuyoshiwada/react-md-spinner#size)
   */
  size: PropTypes.number,
  /**
   * The color of the spinner
   * @see [MDSpinner properties](https://github.com/tsuyoshiwada/react-md-spinner#singleColor)
   */
  color: PropTypes.string,
  /** The message displayed at the bottom of the spinner */
  message: PropTypes.string,
  /** Set position of the spinner backdrop */
  backdropPos: PropTypes.oneOf(['fixed', 'absolute', 'relative'])
}

Spinner.defaultProps = {
  size: 60,
  color: 'white',
  message: '',
  backdropPos: 'absolute'
}
