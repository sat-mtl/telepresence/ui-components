import Bar from './Bar'
import ExtraButton from './ExtraButton'
import Panel from './Panel'
import ScrollTabs from './ScrollTabs'
import Tab from './Tab'
import Tabs from './Tabs'

import './styles.simon.scss'

export default { ExtraButton, Panel, Tab, Tabs, ScrollTabs, Bar }
