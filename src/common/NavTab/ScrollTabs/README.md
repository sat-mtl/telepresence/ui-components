# ScrollTabs example

Create a full tab manager by dynamically adding and removing tabs

```js
import NavTab from '../index.js';
import classNames from 'classnames'
import Icon from '~/common/Icon';
import Button from '~/common/Button';
import FlexColumn from '~/layout/FlexColumn';
import FlexBox from '~/layout/FlexBox';
import ThemeProvider from '~/contexts/Theme';

const { Tab, ExtraButton, Panel } = NavTab;

class TabManager extends React.Component {
  constructor (props) {
    super(props)

    this.$overflow = null

    this.state = {
      hasOverflow: false,
      tabCount: -1,
      tabs: [],
      active: null,
      scrollLeft: 0
    }
  }

  onTabDelete (index) {
    const { tabs } = this.state

    tabs.splice(index, 1)
    this.setState({ tabs: tabs })
  }

  onTabAdd () {
    const { tabs, tabCount } = this.state
    const newCount = tabCount + 1
    const scrollLeft = this.$overflow ? this.$overflow.scrollWidth : 0

    this.setState({
      scrollLeft: scrollLeft,
      tabCount: newCount,
      tabs: [...tabs, newCount]
    })
  }

  renderSceneDeleter (index) {
    return (
      <div className='scenic-scene-delete' onClick={() => this.onTabDelete(index)} />
    )
  }

  renderTab (tab, index) {
    const { active, tabs } = this.state

    return (
      <Tab key={tab} addonAfter={this.renderSceneDeleter(index)}>{`Tab ${tab}`}</Tab>
    )
  }

  renderLeftArrow () {
    const { scrollLeft } = this.state
    const disabled = scrollLeft <= 0

    const scrollToLeft = () => {
      this.setState({ scrollLeft: this.$overflow.scrollLeft - 100 })
    }

    return (
      <ExtraButton onClick={scrollToLeft} disabled={disabled}>
        <div className='scenic-scene-left' />
      </ExtraButton>
    )
  }

  renderRightArrow () {
    const { scrollWidth, offsetWidth } = this.$overflow
    const { scrollLeft } = this.state
    const disabled = (scrollLeft + offsetWidth) >= scrollWidth

    const scrollToRight = () => {
      this.setState({ scrollLeft: this.$overflow.scrollLeft + 100 })
    }

    return (
      <ExtraButton onClick={scrollToRight} disabled={disabled}>
        <div className='scenic-scene-right' />
      </ExtraButton>
    )
  }

  renderSceneAdder () {
    return (
      <ExtraButton onClick={() => this.onTabAdd()}>
        <div className='scenic-scene-add' />
      </ExtraButton>
    )
  }

  handleOverflowChange (hasOverflow, $el) {
    this.$overflow = $el
    this.setState({ hasOverflow: hasOverflow })
  }

  render () {
    const { tabs, hasOverflow, scrollLeft } = this.state
    let addonAfter = null
    let addonBefore = null

    if (hasOverflow) {
      addonBefore = this.renderLeftArrow()

      addonAfter = <>
        {this.renderRightArrow()}
        {this.renderSceneAdder()}
      </>
    }

    return (
      <ScrollTabs
        scrollLeft={scrollLeft}
        onOverflowChange={this.handleOverflowChange.bind(this)}
        addonBefore={addonBefore}
        addonAfter={addonAfter}
      >
        {tabs.map((tab, index) => this.renderTab(tab, index))}
        {addonAfter ? null : this.renderSceneAdder()}
      </ScrollTabs>
    )
  }
}

function ArrowButton ({ disabled, onClick, type }) {
  const cn = classNames(
    'arrow-button',
    { 'disabled': disabled }
  )

  const onArrowClick = () => {
    if (!disabled) onClick()
  }

  return (
    <div className={cn} onClick={onArrowClick}>
      <Icon type={type} />
    </div>
  )
}

function CloseButton ({ onClick }) {
  return (
    <div className='close-button' onClick={onClick}>
      <Icon type='close' />
    </div>
  )
}

class AltTabManager extends TabManager {
  renderLeftArrow () {
    const { scrollLeft } = this.state
    const disabled = scrollLeft <= 0

    const scrollToLeft = () => {
      this.setState({ scrollLeft: this.$overflow.scrollLeft - 100 })
    }

    return (
      <ExtraButton disabled={disabled}>
        <ArrowButton type='left' disabled={disabled} onClick={scrollToLeft} />
      </ExtraButton>
    )
  }

  renderRightArrow () {
    const { scrollWidth, offsetWidth } = this.$overflow
    const { scrollLeft } = this.state
    const disabled = (scrollLeft + offsetWidth) >= scrollWidth

    const scrollToRight = () => {
      this.setState({ scrollLeft: this.$overflow.scrollLeft + 100 })
    }

    return (
      <ExtraButton disabled={disabled}>
        <ArrowButton type='right' disabled={disabled} onClick={scrollToRight} />
      </ExtraButton>
    )
  }

  renderSceneDeleter (index) {
    return (
      <CloseButton onClick={() => this.onTabDelete(index)} />
    )
  }

  renderSceneAdder () {
    return (
      <ExtraButton>
        <Button className='scene-adder' shape='circle' type='subtle' onClick={() => this.onTabAdd()}>
          <div style={{ width: '32px', height: '32px' }}>
            <Icon type='plus' />
          </div>
        </Button>
      </ExtraButton>
    )
  }
}

<div>
  <FlexColumn rowGap={14}>
    <div style={{ backgroundColor: '#2c2c2c' }}>
      <TabManager />
    </div>
    <div style={{ backgroundColor: '#121212' }}>
      <ThemeProvider value='simon'>
        <AltTabManager />
      </ThemeProvider>
    </div>
  </FlexColumn>
</div>
```
