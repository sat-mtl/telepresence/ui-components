import React, { useContext } from 'react'

import PropTypes from 'prop-types'
import classNames from 'classnames'

import { ThemeContext } from '~/contexts/Theme'

/**
 * Bar used to display a navigation for Tab
 * @private
 */
function Bar ({ barRef, children, addonBefore, addonAfter }) {
  const theme = useContext(ThemeContext)

  const cn = classNames('navtab-bar', `navtab-bar-${theme}`)
  const cnAddonAfter = classNames(cn, 'navtab-bar-addon-after')
  const cnAddonBefore = classNames(cn, 'navtab-bar-addon-before')

  return (
    <div className='navtab-bar-wrapper'>
      {addonBefore ? <ul className={cnAddonBefore}>{addonBefore}</ul> : null}
      <ul className={cn} ref={barRef}>{children}</ul>
      {addonAfter ? <ul className={cnAddonAfter}>{addonAfter}</ul> : null}
    </div>
  )
}

Bar.propTypes = {
  /** Tab or ExtraButton components as an array */
  children: PropTypes.any.isRequired,
  /** Reference on the HTMLElement of the bar */
  barRef: PropTypes.any,
  /** Add an extra bar of components before the Tab bar */
  addonBefore: PropTypes.any,
  /** Add an extra bar of components after the Tab bar */
  addonAfter: PropTypes.any
}

export default Bar
