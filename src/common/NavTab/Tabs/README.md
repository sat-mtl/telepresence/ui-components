# Tabs example

## Exemple with Panels

Create all tabs with their associated panel (traditional way to use tabs, see [react-tabs](https://github.com/reactjs/react-tabs)).

```js
import NavTab from '../index.js';
const { Tab, Panel } = NavTab;

<div style={{ backgroundColor: '#2c2c2c' }}>
  <Tabs>
    <Panel tab='Mario'>Mario</Panel>
    <Panel tab='Luigi'>Luigi</Panel>
    <Panel tab='Batman'>Batman</Panel>
  </Tabs>
</div>
```

## Example with trigger event

Create only tabs without panel and use `onClick` to trigger some events.

```js
import NavTab from '../index.js';
const { Tab, Panel } = NavTab;

<div style={{ backgroundColor: '#2c2c2c' }}>
  <Tabs>
    <Tab onClick={() => console.log('Mario')}>Mario</Tab>
    <Tab onClick={() => console.log('Luigi')}>Luigi</Tab>
    <Tab onClick={() => console.log('Batman')}>Batman</Tab>
  </Tabs>
</div>
```

## Exemple with Addons and ExtraButtons

Create tabs with addons: an addon *before* the tab title and another one *after* the tab title.

```js
import NavTab from '../index.js';
import FlexColumn from '~/layout/FlexColumn';
import Icon from '~/common/Icon';
import Button from '~/common/Button';
import Checkbox from '~/inputs/Checkbox';
import ThemeProvider from '~/contexts/Theme';

const { Tab, ExtraButton, Panel } = NavTab;

function onAddonClick (event) {
  event.stopPropagation()
  event.preventDefault()
  console.log('Addon is clicked')
}

function LegacyTabs () {
  return (
    <Tabs>
      <Tab addonBefore={<div className='scenic-scene-active' />}>Mario</Tab>
      <Tab onClick={() => console.log('Tab is clicked')} addonAfter={
        <div className='scenic-scene-delete' onClick={onAddonClick} />
      }>Luigi</Tab>
      <ExtraButton onClick={() => console.log('ExtraButton `scene-left` is clicked')}>
        <div className='scenic-scene-left' />
      </ExtraButton>
      <ExtraButton onClick={() => console.log('ExtraButton `scene-right` is clicked')}>
        <div className='scenic-scene-right' />
      </ExtraButton>
      <ExtraButton onClick={() => console.log('ExtraButton `scene-add` is clicked')}>
        <div className='scenic-scene-add' />
      </ExtraButton>
    </Tabs>
  )
}

function SimonTabs () {
  return (
    <Tabs>
      <Tab addonBefore={
        <Checkbox size='tiny' type='active' checked disabled />
      }>Mario</Tab>
      <Tab addonAfter={
        <div className='close-button'>
          <Icon type='close' />
        </div>
      }>Luigi</Tab>
      <ExtraButton>
        <div className='arrow-button'>
          <Icon type='left' />
        </div>
      </ExtraButton>
      <ExtraButton>
        <div className='arrow-button'>
          <Icon type='right' />
        </div>
      </ExtraButton>
      <ExtraButton>
        <Button className='scene-adder' shape='circle' type='subtle'>
          <div style={{ width: '32px', height: '32px' }}>
            <Icon type='plus' />
          </div>
        </Button>
      </ExtraButton>
    </Tabs>
  )
}

<div>
  <FlexColumn rowGap={14}>
    <div style={{ backgroundColor: '#2c2c2c' }}>
      <LegacyTabs />
    </div>
    <div style={{ backgroundColor: '#121212' }}>
      <ThemeProvider value='simon'>
        <SimonTabs />
      </ThemeProvider>
    </div>
  </FlexColumn>
</div>
```

## Examples with *Click* and *DblClick* handlers

**Click** and **dblclick** events can be handled: you can rename `Double Click` tab by *double clicking* on it. Press `Enter` in order to rename it.

```js
import NavTab from '../index.js';
const { Tab, ExtraButton, Panel } = NavTab;

const click = 'click';
const dblclick = 'dblclick';

class EventTabs extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      rename: null,
      dblclickName: null,
      clickCount: 0
    }
  }

  renderClick () {
    const { rename, clickCount } = this.state
    let clickTab = 'Click'

    if (clickCount > 0) {
      clickTab = `Click ${clickCount}`
    }

    return (
      <Tab onClick={() => this.setState({ clickCount: clickCount + 1 })}>
        {clickTab}
      </Tab>
    )
  }

  renderDblClick () {
    const { rename, dblclickName } = this.state
    let dblclickTab = 'Double Click'

    if (rename === dblclick) {
      dblclickTab =
        <input
          onKeyDown={(e) => {
            if (e.keyCode === 13) this.setState({ dblclickName: e.target.value, rename: null })
          }}
          onBlur={() => { this.setState({ rename: null }) }}
          placeholder={'Rename Me'}
        />
    } else if (dblclickName) {
      dblclickTab = dblclickName
    }

    return (
      <Tab onDoubleClick={() => this.setState({ rename: dblclick })}>
        {dblclickTab}
      </Tab>
    )
  }

  render () {
    return (
      <Tabs>
        {this.renderClick()}
        {this.renderDblClick()}
      </Tabs>
    )
  }
}

<div style={{ backgroundColor: '#2c2c2c' }}>
  <EventTabs />
</div>
```
