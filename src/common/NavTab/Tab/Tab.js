import React, { useContext } from 'react'

import PropTypes from 'prop-types'
import classNames from 'classnames'

import { ThemeContext } from '~/contexts/Theme'

function renderAddon (addon) {
  return !addon ? null : <div className='navtab-tab-addon'>{addon}</div>
}

/** Tab component used to display multiple pages or contents */
function Tab ({ onClick, onDoubleClick, disabled, addonBefore, addonAfter, selected, className, style, children }) {
  const theme = useContext(ThemeContext)
  const cn = classNames(
    'navtab-tab',
    `navtab-tab-${theme}`,
    { 'navtab-tab-disabled': disabled },
    { 'navtab-tab-selected': selected },
    className)

  return (
    <li className={cn} onClick={onClick} onDoubleClick={onDoubleClick} style={style}>
      {renderAddon(addonBefore)}
      <div className='navtab-tab-title'>
        {children}
      </div>
      {renderAddon(addonAfter)}
    </li>
  )
}

Tab.propTypes = {
  /** Gets called when the Tab is clicked by the user */
  onClick: PropTypes.func,
  /** Gets called when the Tab is double clicked by the user */
  onDoubleClick: PropTypes.func,
  /** Flag used to set the Tab in the `selected` state */
  selected: PropTypes.bool,
  /** HTML elements usually used for the Tab title */
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
  /** Add extra HTML before the tab name */
  addonBefore: PropTypes.node,
  /** Add extra HTML after the tab name */
  addonAfter: PropTypes.node,
  /** Add an extra class name to the pre-difined classes */
  className: PropTypes.string,
  /** Flag used to disable the Tab */
  disabled: PropTypes.bool
}

Tab.defaultProps = {
  onClick: Function.prototype,
  addonBefore: null,
  addonAfter: null,
  className: null,
  disabled: false
}

export default Tab
