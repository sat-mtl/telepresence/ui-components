import React, { useContext } from 'react'

import PropTypes from 'prop-types'
import classNames from 'classnames'

import Tab from '../Tab'

import { ThemeContext } from '~/contexts/Theme'

/** Button component used to add functionnality into a Tab bar */
function ExtraButton ({ disabled, onClick, children }) {
  const theme = useContext(ThemeContext)
  const cn = classNames('navtab-extra-button', `navtab-extra-button-${theme}`)

  return (
    <Tab className={cn} onClick={onClick} disabled={disabled}>
      {children}
    </Tab>
  )
}

ExtraButton.propTypes = {
  /** Flag used to disable the ExtraButton */
  disabled: PropTypes.bool,
  /** Sets a function which is triggered when the button is clicked */
  onClick: PropTypes.func,
  /** Title of the button */
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired
}

ExtraButton.defaultProps = {
  onClick: Function.prototype,
  disabled: false
}

export default ExtraButton
