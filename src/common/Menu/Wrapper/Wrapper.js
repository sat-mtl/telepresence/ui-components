import React, { useContext, Children, cloneElement } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import { ThemeContext } from '~/contexts/Theme'

/** Abstract Scenic menu which displays a list of HTML `li` elements */
function Wrapper ({ onBlur, menuRef, top, width, accordion, children, expanded }) {
  const theme = useContext(ThemeContext)

  const className = classNames(
    'Wrapper',
    `Wrapper-${theme}`,
    { WrapperTop: top },
    { [`Wrapper-${theme}-top`]: top }
  )

  const style = {
    width: width,
    display: expanded ? 'initial' : 'none'
  }

  const childrenWithProps = Children.map(
    children,
    (child) => cloneElement(child, {
      width: width,
      accordion: accordion
    })
  )

  return (
    <ul
      ref={menuRef}
      style={style}
      onBlur={onBlur}
      className={className}
    >
      {childrenWithProps}
    </ul>
  )
}

Wrapper.propTypes = {
  /** Flag to set the menu as absolute */
  top: PropTypes.bool,
  /** Width of the menu */
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  /** HTML elements as a list */
  children: PropTypes.any,
  /** Triggers action when the menu lose the focus */
  onBlur: PropTypes.func,
  /** Reference of the menu element, used to set focus after some actions */
  menuRef: PropTypes.any,
  /** Flag which triggers an accordion animation when the menu is expanded */
  accordion: PropTypes.bool,
  /** Flag that displays the wrapped menu */
  expanded: PropTypes.bool
}

Wrapper.defaultProps = {
  width: 180,
  top: false,
  onBlur: Function.prototype,
  accordion: false,
  expanded: true
}

export default Wrapper
