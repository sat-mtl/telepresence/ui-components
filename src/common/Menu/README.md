## Simple menus

The Menu kit is useful to represent tree based data as a menu. The UI is declared as :

1. A `Menu` component that encapsulates the menu under a button
2. A `Wrapper` component that wraps its children and diffuses all its props
3. A `Group` component that orders data in a sub-menu
4. An `Item` component that represents a leaf of the data tree.

```js
import React, { useState } from 'react';
import MenuKit from './index.js';

import ThemeProvider from '~/contexts/Theme';
import FlexRow from '~/layout/FlexRow'
import Icon from '~/common/Icon';

const { Menu, Group, Item } = MenuKit

let showGroup = false;

const showOnClick = () => {
  showGroup = !showGroup;
};

const rootStyle = {
  position: 'absolute',
  zIndex: 900,
  width: 180
}

function MenuExemple ({ title, addonBefore, addonAfter, groupAddon }) {
  const [showGroup1, toggleGroup1] = useState(false);
  const onClick1 = () => toggleGroup1(!showGroup1);

  const [showGroup2, toggleGroup2] = useState(false);
  const onClick2 = () => toggleGroup2(!showGroup2);

  const [expandMenu, toggleMenu] = useState(false);
  const onClickMenu = () => toggleMenu(!expandMenu);

  const menuProps = {
    title: title,
    addonAfter: addonAfter,
    addonBefore: addonBefore,
    onClick: onClickMenu,
    width: 140,
    accordion: true
  }

  return (
    <Menu expanded={expandMenu} {...menuProps}>
      <Group
        title='Group1'
        addonAfter={groupAddon}
        showGroup={showGroup1}
        onClick={onClick1}
      >
        <Item>item-11</Item>
        <Item>item-12</Item>
        <Item>item-13</Item>
      </Group>
      <Group
        title='Group2'
        addonBefore={groupAddon}
        showGroup={showGroup2}
        onClick={onClick2}
      >
        <Item>item-21</Item>
        <Item>item-22</Item>
        <Item>item-23</Item>
      </Group>
      <Item>item2</Item>
      <Item>item3</Item>
    </Menu>
  )
}

<div style={{ width: '100%', height: '100%', backgroundColor: '#222' }}>
  <div style={{ padding: 10 }} >
    <FlexRow columnGap={8}>
      <ThemeProvider value='simon'>
        <MenuExemple
          title='Sources'
          addonBefore={<Icon type='plus' />}
        />
        <MenuExemple
          title='Filtres'
          groupAddon={<Icon type='plus' />}
          addonAfter={'▼'}
        />
      </ThemeProvider>
    </FlexRow>
  </div>
</div>
```

## Menus with fields

This menu kit can also be used to represent more complex UI such as collapsible forms. The `Item` property `type` should be change to `panel` in order to have a more consistent UI.

```js { "props": { "style": { "width": "100%", "height": "100%", "backgroundColor": "#222" }}}
import React, { useState } from 'react'
import MenuKit from './index.js'

import ThemeProvider from '~/contexts/Theme'
import FlexRow from '~/layout/FlexRow'
import Icon from '~/common/Icon'
import Inputs from '~/inputs'

const { Field, InputText, InputNumber } = Inputs
const { Wrapper, Group, Item } = MenuKit

let showGroup = false

const showOnClick = () => {
  showGroup = !showGroup
}

const [showGroup1, toggleGroup1] = useState(false)
const onClick1 = () => toggleGroup1(!showGroup1)

const [showGroup2, toggleGroup2] = useState(false)
const onClick2 = () => toggleGroup2(!showGroup2)

const [expandMenu, toggleMenu] = useState(false)
const onClickMenu = () => toggleMenu(!expandMenu)

const [text11, setText11] = useState(undefined)
const [number12, setNumber12] = useState(0)
const [text13, setText13] = useState(undefined)

const wrapperProps = {
  onClick: onClickMenu,
  accordion: true,
  width: '100%'
}

;<Wrapper {...wrapperProps}>
  <Group title='Group1' showGroup={showGroup1} onClick={onClick1}>
    <Item type='panel'>
      <Field title='I am an InputText'>
        <InputText
          value={text11}
          onChange={(e) => { setText11(e.target.value) }} />
      </Field>
    </Item>
    <Item type='panel'>
      <Field title='I am an InputNumber'>
        <InputNumber
          value={number12}
          onChange={(v) => { setNumber12(v) }} />
      </Field>
    </Item>
    <Item type='panel'>
      <Field title='I am an InputText'>
        <InputText
          value={text13}
          onChange={(e) => { setText13(e.target.value) }} />
      </Field>
    </Item>
  </Group>
  <Group title='Group2' showGroup={showGroup2} onClick={onClick2}>
    <Item type='panel'>
      <Field title='I am an InputText'>
        <InputText
          value={text11}
          onChange={(e) => { setText11(e.target.value) }} />
      </Field>
    </Item>
    <Item type='panel'>
      <Field title='I am an InputNumber'>
        <InputNumber
          value={number12}
          onChange={(v) => { setNumber12(v) }} />
      </Field>
    </Item>
    <Item type='panel'>
      <Field title='I am an InputText'>
        <InputText
          value={text13}
          onChange={(e) => { setText13(e.target.value) }} />
      </Field>
    </Item>
  </Group>
</Wrapper>
```
