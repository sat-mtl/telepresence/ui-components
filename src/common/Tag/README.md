<!-- markdownlint-disable MD041 -->

All statuses from the StatusEnum must be used in order to highlight data.

```js
import React, { useState } from 'react'
import FlexRow from '~/layout/FlexRow'
import Tag from './index';

<FlexRow flexWrap='wrap' columnGap={4}>
  <Tag>'tag'</Tag>
  <Tag status='focus'>
    focus
  </Tag>
  <Tag status='active'>
    active
  </Tag>
  <Tag status='danger'>
    danger
  </Tag>
  <Tag status='busy'>
    busy
  </Tag>
</FlexRow>
```

A custom color could also be used but it will be overrided by the status if the both are used.

```js
import React, { useState } from 'react'
import FlexRow from '~/layout/FlexRow'
import Tag from './index';

<FlexRow flexWrap='wrap' columnGap={4}>
  <Tag color='magenta'>
    magenta
  </Tag>
  <Tag color='#00FFFF'>
    cyan
  </Tag>
  <Tag color='#fff' status='focus'>
    white
  </Tag>
</FlexRow>
```

Addons must be added manually in order to add icons before or after the tag label. Also, an `onClick` handler should be added if you want to trigger something when the component is clicked,

```js
import React, { useState } from 'react'
import FlexRow from '~/layout/FlexRow'
import Tag from './index';
import Icon from '../Icon';

const $close = (
  <div style={{ height: '8px', width: '8px' }}>
    <Icon type='close'/>
  </div>
);

<FlexRow flexWrap='wrap' columnGap={4}>
  <Tag status='active' addonBefore='+' onClick={() => console.log('active')}>
    add
  </Tag>
  <Tag status='danger' addonAfter={$close} onClick={() => console.log('danger')}>
    close
  </Tag>
</FlexRow>
```
