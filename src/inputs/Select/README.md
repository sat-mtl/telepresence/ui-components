<!-- markdownlint-disable MD041 -->

```js
import React, { useState } from 'react';

import Select from './index'

import ThemeProvider from '~/contexts/Theme';
import FlexColumn from '~/layout/FlexColumn';

const options = [{
  label: 'Item 1',
  value: 'item1'
}, {
  label: 'Item 2',
  value: 'item2'
}, {
  label: 'Item 3',
  value: 'item3'
}, {
  label: 'Item 4',
  value: 'item4'
}];

function ExampleSelect ({ status }) {
  const [selected, setSelection] = useState({})

  return (
    <Select
      options={options}
      selected={selected}
      status={status}
      onSelection={(value) => {
        console.log(value)
        const item = options.find(item => item.label == value);

        setSelection({
          label: item.label,
          value: item.value
        })
      }}
    />
  )
}

<div style={{ width: '100%', height: '100%', backgroundColor: '#222' }}>
  <div style={{ padding: 10 }}>
    <FlexColumn rowGap={8}>
      <ThemeProvider value='simon'>
        <ExampleSelect />
        <ExampleSelect status='danger' />
        <Select options={options} disabled />
      </ThemeProvider>
    </FlexColumn>
  </div>
</div>
```

An example with extreme an use case : a too long collection with too long labels... This example uses a component with state in order to update the selected item.

```js
import { Component } from "react";

import FlexColumn from '~/layout/FlexColumn';
import ThemeProvider from '~/contexts/Theme';

import Select from './index';

const options = 'abcdefghijklmnopqrstuvwxyz'.split('').map(letter => ({
  label: letter.toUpperCase().repeat(100),
  value: letter.repeat(100)
}));

class Example extends Component {
  constructor () {
    this.state = { selected: {} }
  }

  onSelection (value) {
    const item = options.find(item => item.label == value);

    this.setState({
      selected: {
        label: item.label,
        value: item.value
      }
    })
  }

  render() {
    const { selected } = this.state

    return (
      <Select
        hasError={Object.keys(selected).length === 0}
        options={options}
        place='bottom'
        selected={this.state.selected}
        onSelection={this.onSelection.bind(this)}
      />
    )
  }
}

<FlexColumn rowGap={8}>
  <Example />
</FlexColumn>
```

The `place` and the `size` properties can be used to change the place and the size of the Select menu (not supported with *legacy* theme).

```js
import React, { useState } from 'react';

import Select from './index'

import ThemeProvider from '~/contexts/Theme';
import FlexRow from '~/layout/FlexRow';

const options = [{
  label: 'Item 1',
  value: 'item1'
}, {
  label: 'Item 2',
  value: 'item2'
}, {
  label: 'Item 3',
  value: 'item3'
}, {
  label: 'Item 4',
  value: 'item4'
}];

function ExampleSelect ({ place, placeholder, size }) {
  const [selected, setSelection] = useState({})

  return (
    <div style={{ width: '100%' }}>
      <Select options={options}
        place={place}
        size={size}
        placeholder={placeholder}
        selected={selected}
        onSelection={(value) => {
          const item = options.find(item => item.label == value);

          setSelection({
            label: item.label,
            value: item.value
          })
        }}/>
    </div>
  )
}

<div style={{ width: '100%', height: '100%', backgroundColor: '#222' }}>
  <div style={{ padding: 10 }}>
    <FlexRow columnGap={8} justifyContent='space-between' >
      <ThemeProvider value='simon'>
        <ExampleSelect place='bottom' placeholder='Bottom' />
        <ExampleSelect place='bottom' placeholder='Bottom' size='tiny' />
        <ExampleSelect place='top' placeholder='Top' />
        <ExampleSelect place='top' placeholder='Top' size='tiny' />
      </ThemeProvider>
    </FlexRow>
  </div>
</div>
```
