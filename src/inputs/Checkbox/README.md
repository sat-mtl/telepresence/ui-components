<!-- markdownlint-disable MD041 -->

The Checkboxes are used to check some data. They are based on [the HTML specification](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Input/checkbox).

```js static
import React, { useState } from 'react';
import Checkbox, { types, sizes, shapes } from './Checkbox.js';
import ThemeProvider from '~/contexts/Theme';

function Example ({ disabled, status, size, shape, children }) {
  return (
    <Checkbox
      status={status}
      size={size}
      shape={shape}
      checked={isChecked}
      disabled={disabled}
      onChange={(e, checked) => { toggleCheckbox(checked) }}
    >
      {children}
    </Checkbox>
  )
}
```

You can use all the statuses defined by the StatusEnum.

```js { "props": { "style": { "width": "100%", "height": "100%", "backgroundColor": "#222" }}}
import React, { useState } from 'react'
import Checkbox, { types, sizes, shapes } from './Checkbox.js'

import FlexRow from '~/layout/FlexRow'
import StatusEnum from '~/common/StatusEnum'

const $statuses = Object.values(StatusEnum).map(status => {
  const [isChecked, setCheck] = useState(false)

  return (
    <Checkbox
      key={status}
      checked={isChecked}
      onChange={() => setCheck(!isChecked)}
      status={status} />
  )
})

;<div style={{ padding: 10 }} >
  <FlexRow columnGap={8}>
    <Checkbox
      key='readonly'
      checked
    />
    {$statuses}
  </FlexRow>
</div>
```

There are two extra properties:
  - `size` that includes the `tiny`, `normal` and `large` options
  - `shape` that includes the `square` and `circle` options

```js { "props": { "style": { "width": "100%", "height": "100%", "backgroundColor": "#222" }}}
import React, { useState } from 'react'
import Checkbox, { types, sizes, shapes } from './Checkbox.js'

import FlexRow from '~/layout/FlexRow'
import FlexColumn from '~/layout/FlexColumn'

const [isTooMuch, setTooMuch] = useState(false)

const $sizes = sizes.map(size => {
  const [isChecked, setCheck] = useState(false)

  return (
    <Checkbox
      key={size}
      checked={isChecked}
      onChange={() => setCheck(!isChecked)}
      size={size} />
  )
})

const $shapes = shapes.map(shape => {
  const [isChecked, setCheck] = useState(false)

  return (
    <Checkbox
      key={shape}
      checked={isChecked}
      onChange={() => setCheck(!isChecked)}
      shape={shape} />
  )
})

;<div style={{ padding: 10 }} >
<FlexColumn rowGap={8}>
  <FlexRow columnGap={8} alignItems='center'>
    {$sizes}
  </FlexRow>
  <FlexRow columnGap={8} alignItems='center'>
    {$shapes}
    <Checkbox
      key='toomuch'
      type='active'
      size='large'
      checked={isTooMuch}
      onChange={() => setTooMuch(!isTooMuch)}
      shape='circle'>
      &#x2600;
    </Checkbox>
  </FlexRow>
  </FlexColumn>
</div>
```
# WIP gabrielle styles

```js
import React, { useState } from 'react'
import Checkbox, { types, sizes, shapes } from './Checkbox.js'
import Icon from '~/common/Icon/Icon.js'

import FlexRow from '~/layout/FlexRow'
import ThemeProvider from '~/contexts/Theme'

const connectorsColors = ['#488854', '#EC9717', '#8331A0']

const $connectors = connectorsColors.map(color => {
  const [isChecked, setCheck] = useState(false)

  return (
    <Checkbox
      key={color}
      checked={isChecked}
      onChange={() => setCheck(!isChecked)}
      color={color}
      shape='circle'
      size='xlarge'
    >
        <div style={{width: '1rem', height: '0,75rem'}}>
          <Icon type='check' />
        </div>
    </Checkbox>
  )
})

;<div style={{ padding: 10, backgroundColor: '#121212' }} >
<ThemeProvider value='gabrielle'>
  <FlexRow columnGap={16}>
    {$connectors}
  </FlexRow>
  </ThemeProvider>
</div>
```
