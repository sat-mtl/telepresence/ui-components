import React, { useContext } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import { createUseStyles } from 'react-jss'

import StatusEnum from '~/common/StatusEnum'
import { ThemeContext } from '~/contexts/Theme'

import { convertToPercentage, getDigits, validateNumber } from '~/utils/numberTools'
import { getOffsetValue, getKeyDownValue, getWheelValue, getClosestElement } from '~/utils/htmlTools'

import Rail from './Rail.js'

/**
 * Defines dynamic styles for Slider
 * @private
 */
const useStyles = createUseStyles({
  track: {
    width: ({ min, max, value }) => `${convertToPercentage(min, max, value)}%`
  },
  handle: {
    left: props => `calc(${props.percent}% - 5px)`
  }
})

/**
 * Gets the class name of the root component
 * @private
 * @param {string} theme - Current theme of the component
 * @param {boolean} disabled - Flags a disabled component
 * @param {string} status - Status of the component
 * @returns {string} A concatenated class name
 */
function getRootClass ({ theme, disabled, status }) {
  return classNames(
    'Slider',
    `Slider-${theme}`,
    { 'Slider-disabled': disabled },
    { [`Slider-${status}`]: status && !disabled }
  )
}

/**
 * Gets the class name of the track
 * @private
 * @param {string} styleClasses - Classes of the jss styles for the track
 * @returns {string} Class names of the track
 */
function getTrackClass (styleClasses) {
  return classNames('SliderTrack', styleClasses)
}

/**
 * The Slider is a controlled component which let users slide a track on the X axis
 * It reacts to `onKeyDown`, `onWheel`, `onMouseMove` and `onClick` events.
 */
function Slider (props) {
  const theme = useContext(ThemeContext)
  const styles = useStyles(props)

  const { disabled, value, min, max, step, onChange } = props

  const handleValueChange = (newValue) => onChange(
    validateNumber(min, max, value, newValue, getDigits(step))
  )

  const rootProps = {
    className: getRootClass({ ...props, theme }),
    tabIndex: 0,
    onWheel: (event) => {
      if (!disabled && event.nativeEvent && event.nativeEvent.shiftKey && event.preventDefault) {
        event.preventDefault()

        handleValueChange(
          getWheelValue(event.wheel, value, step)
        )
      }
    },
    onKeyDown: (event) => {
      if (!disabled && event.preventDefault) {
        event.preventDefault()

        handleValueChange(
          getKeyDownValue(event.key, value, step)
        )
      }
    }
  }

  const railProps = {
    disabled: disabled,
    onSlide: (event) => {
      if (!disabled && event.nativeEvent && event.preventDefault) {
        const { nativeEvent: { target, offsetX } } = event
        const $target = getClosestElement(target, '.SliderRail') || target

        event.preventDefault()

        handleValueChange(
          getOffsetValue($target.offsetWidth, offsetX, value, min, max)
        )
      }
    }
  }

  return (
    <div {...rootProps}>
      <input type='range' {...props} style={{ display: 'none' }} />
      <Rail {...railProps}>
        <div className={getTrackClass(styles.track)} />
      </Rail>
    </div>
  )
}

Slider.propTypes = {
  /** If `true`, the input will be disabled */
  disabled: PropTypes.bool,

  /** Minimal allowed value */
  min: PropTypes.number,

  /** Maximum allowed value */
  max: PropTypes.number,

  /** Triggered when the value of the slider change */
  onChange: PropTypes.func,

  /** Current status of the component */
  status: PropTypes.oneOf(Object.values(StatusEnum)),

  /** Step value for each slides */
  step: PropTypes.number,

  /** Current value of the component */
  value: PropTypes.number
}

Slider.defaultProps = {
  onChange: Function.prototype,
  disabled: false,
  min: 0,
  max: 100,
  step: 1,
  value: 0
}

export default Slider
