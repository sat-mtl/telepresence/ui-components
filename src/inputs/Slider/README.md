<!-- markdownlint-disable MD041 -->

The `slider` input supports several events:

+ `onclick` event with **onClick** property
+ `onmousewheel` event with **onWheel** property
+ `onkeydown` event with **onKeyPress** property

```js
import React, { useState } from 'react';
import Slider from './index'
import ThemeProvider from '~/contexts/Theme';
import FlexColumn from '~/layout/FlexColumn';

function DynamicSlider ({ ...props }) {
  const [value, setValue] = useState(props.value || 50)

  return (
    <Slider {...props} value={value} onChange={(newValue) => { setValue(newValue) }} />
  )
}

<div style={{ width: '100%', height: '100%', backgroundColor: '#222' }}>
  <div style={{ padding: 10 }} >
    <FlexColumn rowGap={8}>
      <DynamicSlider />
      <DynamicSlider status='busy' value={70} />
      <DynamicSlider status='danger' value={90} />
      <DynamicSlider disabled value={20} />
    </FlexColumn>
  </div>
</div>
```

The Slider component can be combined with an **InputNumber** component.

```js
import React, { useState } from 'react';
import Slider from './index'
import ThemeProvider from '~/contexts/Theme';
import Number from '~/inputs/InputNumber';
import FlexColumn from '~/layout/FlexColumn';

function ValueSelector ({ ...props }) {
  const [value, setValue] = useState(props.value || 0)

  const common = {
    value: value,
    min: -0.1,
    max: 0.1,
    step: 0.01,
    onChange: (newValue) => { console.log(newValue); setValue(newValue) }
  }

  const gridLayout = {
    width: '100%',
    display: 'inline-grid',
    gridTemplateColumns: '[slider] auto [input] 180px',
    gridColumnGap: '8px'
  }

  return (
    <div style={{ ...gridLayout }}>
      <span style={{ gridArea: 'slider' }}>
        <Slider {...props} {...common} />
      </span>
      <span style={{ gridArea: 'input' }}>
        <Number {...props} {...common} />
      </span>
    </div>
  )
}

<div style={{ width: '100%', height: '100%', backgroundColor: '#222' }}>
  <div style={{ padding: 10 }} >
    <ThemeProvider value='simon'>
      <FlexColumn rowGap={8}>
        <ValueSelector value={0.025} />
        <ValueSelector disabled />
      </FlexColumn>
    </ThemeProvider>
  </div>
</div>
```
