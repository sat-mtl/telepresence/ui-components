import React, { useContext, useState } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

import StatusEnum from '~/common/StatusEnum'
import { ThemeContext } from '~/contexts/Theme'

import './InputSelect.scss'

/**
 * InputSelect's Addon encapsulates all addons
 * @private
 * @param {string|React.node} addon - An addon before or after the input text
 * @returns {React.PureComponent} An addon component
 */
function Addon ({ addon }) {
  return (
    <span className='InputSelectAddon'>
      {addon}
    </span>
  )
}

/**
 * Gets the class names of the root component
 * @private
 * @param {string} theme - Current theme
 * @param {boolean} disabled - Flag if the field is disabled
 * @param {string} status - Current status of the field
 * @returns {string}  All concatenated class names
 */
function getRootClass ({ theme, disabled, status, rootClassName }) {
  return classNames(
    'InputSelect',
    `InputSelect-${theme}`,
    { 'InputSelect-disabled': disabled },
    { [`InputSelect-${status}`]: status },
    rootClassName
  )
}

/**
 * Gets the class names of the select container field
 * @private
 * @param {string} extraClassName - Extra class name of the field
 * @returns {string} All concatenated class names
 */
function getFieldClass (extraClassName) {
  return classNames(
    'InputSelectField',
    extraClassName
  )
}

/**
 * Gets the properties of the root component
 * @private
 * @param {string} className - Class name of the root component
 * @param {string} theme - Current theme of the component
 * @param {boolean} disabled - Flags if the component is disabled
 * @param {string} status - Current status of the component
 * @returns {Object} All properties of the root component
 */
function getRootProps ({ className, theme, disabled, status }) {
  return {
    className: className,
    alignItems: 'center',
    rowGap: 8
  }
}

/**
 * Gets the properties of the select container
 * @private
 * @param {boolean} disabled - Flag if the component is disabled
 * @param {string} className - Extra class name of the select
 * @param {string} placeholder - Placeholder of the select
 * @param {string} name - Name of the select
 * @param {function} onSelect - Function triggered when the user inputs values
 * @param {function} onFocus - Function triggered when the user focuses the select
 * @returns {Object} All the properties of the select
 */
function getFieldProps ({ disabled, className, placeholder, name }) {
  return {
    disabled: disabled,
    className: className,
    type: 'text',
    placeholder: placeholder,
    name: name
  }
}

/**
 * Renders the selected placeholder or the selected option
 * @param {object} option - An object with all the option's details
 * @param {external:react/Component} addonAfter - An Icon with a specific dropDown type
 * @returns {external:react/Component} - A placeholder or a selected option for the Input Select
 */
function InputSelectPlaceholder ({ option, addonAfter, placeholder }) {
  return (
    <div className='InputSelectPlaceholder'>
      <div className='SelectedItem'>
        {option.label && <span className='SelectedItemAddon'>{option.label()}</span>}
        {option.value === placeholder && <span className='SelectedItemPlaceholder'>{option.value}</span>}
        {option.value !== placeholder && <span className='SelectedItemValue'>{option.value}</span>}
        <span className='SelectedItemArrow'>{addonAfter && <Addon addon={addonAfter} />}</span>
      </div>
    </div>
  )
}

/**
 * Renders the input select's menu or list of options
 * @param {object} options - An array with a number of options as objects
 * @param {string} place - The position of the list container
 * @param {function} onOptionSelected - A function triggered when an option is selected
 * @returns {external:react/Component} A list of options
 */
function InputListContainer ({ options, place, onOptionSelected }) {
  return (
    <div className={`InputSelectListContainer InputSelectListContainer-${place}`}>
      <ul className='InputSelectList'>
        {options.map(opt => <InputListItem option={opt} onOptionSelected={onOptionSelected} key={opt.id} />)}
      </ul>
    </div>
  )
}

/**
 * Renders the input select's option item
 * @param {object} option - An object containing the details of the selected option
 * @param {function} onOptionSelected - A function triggered when an option is selected
 * @returns {external:react/Component} An option list item
 */
function InputListItem ({ option, onOptionSelected }) {
  return (
    <li
      className='InputSelectItem'
      key={option.id}
      onClick={onOptionSelected(option)}
    >
      {option.label && <div className='InputSelectItemLabel Label'>{option.label()}</div>}
      <span className='InputSelectItemValue'>{option.value}</span>
    </li>
  )
}

/**
 * InputSelect is a Controlled Component which lets users enter and edit text
 * @visibleName InputSelect
 * @see See [Controlled Components](https://reactjs.org/docs/forms.html#controlled-components) specification
 */
function InputSelect (props) {
  const theme = useContext(ThemeContext)
  const { disabled, options, placeholder, label, place, addonAfter, onChange } = props

  const [isDisplayed, setIsDisplayed] = useState(false)
  const [selectedOption, setSelectedOption] = useState({ value: placeholder })

  const toggleDropdownDisplay = () => {
    setIsDisplayed(!isDisplayed)
  }

  const onOptionSelected = value => () => {
    setSelectedOption(value)
    setIsDisplayed(false)
    onChange(value)
  }

  const onSelect = () => {
    if (!disabled) toggleDropdownDisplay()
  }

  const rootClass = getRootClass({ ...props, theme })
  const fieldClass = getFieldClass(props.className)

  const rootProps = getRootProps({ ...props, theme, className: rootClass })
  const fieldProps = getFieldProps({ ...props, onSelect, theme, className: fieldClass })

  return (
    <div {...rootProps}>
      <div id='InputSelectContainer' {...fieldProps}>
        <div className='InputSelectHeader' onClick={onSelect}>
          {label && <span className='InputSelectHeaderLabel'>{label}</span>}
          <InputSelectPlaceholder
            id='InputSelectPlaceholder'
            option={selectedOption}
            addonAfter={addonAfter}
            placeholder={placeholder}
          />
        </div>

        {isDisplayed &&
          <InputListContainer
            id='InputListContainer'
            options={options}
            place={place}
            onOptionSelected={onOptionSelected}
          />}
      </div>
    </div>
  )
}

InputSelect.propTypes = {
  /** Root class name of the component */
  rootClassName: PropTypes.string,

  /** Class name for the HTML input */
  className: PropTypes.string,

  /** If `true`, the input will be disabled */
  disabled: PropTypes.bool,

  /** Status of the input */
  status: PropTypes.oneOf(Object.values(StatusEnum)),

  /** The function triggered on user's selection */
  onChange: PropTypes.func,

  /** Short hint displayed in the empty input */
  placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),

  /** Value of the input */
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),

  /** Set an addon component after the input */
  addonAfter: PropTypes.any,

  /** Set an addon component before the input */
  addonBefore: PropTypes.any,

  /** The options array */
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.node])
  }))

}

InputSelect.defaultProps = {
  disabled: false,
  placeholder: 'Select an option...',
  place: 'normal',
  id: undefined,
  status: StatusEnum.FOCUS,
  selected: {},
  size: 'normal',
  onChange: Function.prototype,
  options: []
}

export default InputSelect

export { Addon, getRootClass, getFieldClass }
