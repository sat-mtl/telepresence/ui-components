<!-- markdownlint-disable MD041 -->

## WIP: Gabrielle style
```js

import React, { useState } from 'react';

import InputSelect from './index'

import ThemeProvider from '~/contexts/Theme';
import FlexRow from '~/layout/FlexRow';
import Icon from '~/common/Icon';
import './Label.scss'

const options = [{
  id: 'V1',
  label: () => (
    <span className='HdmiLabel Label'>
        V1
    </span>
  ),
  value: 'HDMI'
}, {
  id: 'G1',
  label: () => (
    <span className='UsbLabel Label'>
        G1
    </span>
  ),
  value: 'USB'
}, {
  id: 'ndi',
  label: () => (
    <span className='NdiLabel Label'>
        <Icon type='ndi'/>
    </span>
  ),
  value: 'NDI'
}, {
  id: 'B1',
  label: () => (
    <span className='BluetoothLabel Label'>
        <Icon type='bluetooth'/>
    </span>
  ),
  value: 'Bluetooth',
}, {
  id: 'M1',
  label: () => (
    <span className='XlrLabel Label'>
        M1
    </span>
  ),
  value: 'XLR-Jack'
 }]

function ExampleSelect ({ place, placeholder, size, label, addonAfter }) {

  return (
    <div style={{ width: '100%' }}>
      <InputSelect options={options}
        place={place}
        size={size}
        placeholder={placeholder}
        label={label}
        addonAfter={addonAfter}
      />
    </div>
  )
}

<div style={{ width: '100%', height: '100%', backgroundColor: '#222' }}>
  <div style={{ padding: 10 }}>
    <FlexRow columnGap={8} justifyContent='space-between' >
      <ThemeProvider value='gabrielle'>
        <ExampleSelect place='right'
        size='normal'
        placeholder="Choisir une prise pour l'appareil"
        label="Branchement"
        addonAfter={<Icon type='dropDown' />}
        />
        <ExampleSelect place='top'
        size='normal'
        placeholder="Choisir une prise pour l'appareil"
        label="Branchement"
        addonAfter={<Icon type='dropDown' />}
        />
        <ExampleSelect place='left'
        size='normal'
        placeholder="Choisir une prise pour l'appareil"
        label="Branchement"
        addonAfter={<Icon type='dropDown' />}
        />
      </ThemeProvider>
    </FlexRow>
  </div>
</div>
```
