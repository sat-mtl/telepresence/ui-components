import React from 'react'
import classNames from 'classnames'

import Button from '~/common/Button'
import Icon from '~/common/Icon'

import { validateNumber, getDigits } from '~/utils/numberTools'

import './InputButton.scss'

/**
 * Increments a value according to its minimum, its maximum and its step
 * @private
 * @param {number} min - Minimal allowed value
 * @param {number} max - Maximum allowed value
 * @param {number} value - Current value to increment
 * @param {number} step - The incremental step
 * @returns {number} The incremented value
 */
function incrementValue (min, max, value, step) {
  return validateNumber(min, max, value, value + step, getDigits(step))
}

/**
 * Gets the class name of the increment InputButton
 * @private
 * @returns {string} The concatenated class name
 */
function getIncrementClass () {
  return classNames(
    'InputButton',
    'InputButtonIncrement'
  )
}

/**
 * Gets the properties of the increment InputButton
 * @private
 * @param {number} min - Minimal allowed value
 * @param {number} max - Maximum allowed value
 * @param {number} value - Current value to increment
 * @param {number} step - The incremental step
 * @param {function} onChange - Callback when the value is changed
 * @returns {object} Some properties of the InputButton
 */
function getIncrementProps ({ min, max, value, step, onChange }) {
  return {
    className: getIncrementClass(),
    type: 'plus',
    onClick: () => {
      onChange(incrementValue(min, max, value, step))
    }
  }
}

/**
 * Decrements a value according to its minimum, its maximum and its step
 * @private
 * @param {number} min - Minimal allowed value
 * @param {number} max - Maximum allowed value
 * @param {number} value - Current value to decrement
 * @param {number} step - The decremental step
 * @returns {number} The decremented value
 */
function decrementValue (min, max, value, step) {
  return validateNumber(min, max, value, value - step, getDigits(step))
}

/**
 * Gets the class name of the decrement InputButton
 * @private
 * @returns {string} The concatenated class name
 */
function getDecrementClass () {
  return classNames(
    'InputButton',
    'InputButtonDecrement'
  )
}

/**
 * Gets the properties of the decrement InputButton
 * @private
 * @param {number} min - Minimal allowed value
 * @param {number} max - Maximum allowed value
 * @param {number} value - Current value to increment
 * @param {number} step - The incremental step
 * @param {function} onChange - Callback when the value is changed
 * @returns {object} Some properties of the InputButton
 */
function getDecrementProps ({ min, max, value, step, onChange }) {
  return {
    className: getDecrementClass(),
    type: 'minus',
    onClick: () => {
      onChange(decrementValue(min, max, value, step))
    }
  }
}

/**
 * Gets the properties of the inner button
 * @private
 * @todo Implements the busy/warning status
 * @param {boolean} disabled - Flags a disabled button
 * @param {string} status - Status of the component
 * @returns {object} All the properties of the button
 */
function getButtonProps ({ disabled, status }) {
  return {
    shape: 'square',
    disabled: disabled,
    type: status === 'danger' ? 'danger' : 'primary'
  }
}

/**
 * Component used for the InputNumber buttons
 * @private
 */
function InputButton ({ className, type = 'plus', disabled, status, onClick }) {
  return (
    <span className={className}>
      <Button {...getButtonProps({ disabled, status })} onClick={onClick}>
        <div className='InputButtonSymbol'>
          <Icon type={type} />
        </div>
      </Button>
    </span>
  )
}

/**
 * InputButton that decrements values
 * @private
 */
export function InputButtonDecrement (props) {
  return (
    <InputButton {...props} {...getDecrementProps(props)} />
  )
}

/**
 * InputButton that increments values
 * @private
 */
export function InputButtonIncrement (props) {
  return (
    <InputButton {...props} {...getIncrementProps(props)} />
  )
}

export default InputButton
