<!-- markdownlint-disable MD041 -->

```js
import React, { useState } from 'react';

import Number from './index';

import ThemeProvider from '~/contexts/Theme';
import FlexColumn from '~/layout/FlexColumn';

function ExampleInput () {
  const [value, setValue] = useState(0)

  return (
    <Number
      value={value}
      onChange={(v) => { console.log(`changed value: ${v}`); setValue(v) }}
    />
  )
};

function ErrorInput () {
  const [value, setValue] = useState(0)

  return (
    <Number
      value={value}
      status="danger"
      onChange={(v) => { console.log(`changed value: ${v}`); setValue(v) }}
    />
  )
};

<div style={{ width: '100%', height: '100%', backgroundColor: '#222222' }}>
  <div style={{ padding: 10 }}>
    <FlexColumn rowGap={8}>
      <ThemeProvider value='simon'>
        <ExampleInput />
        <ErrorInput />
        <Number value={666} disabled />
        <Number value={0} step={0.01} withButtons={false} />
      </ThemeProvider>
    </FlexColumn>
  </div>
</div>
```

**Be careful**, the inputed value is always checked as a number. Consequently if the user inputs invalid values, the field will be reset to the initial value.

```js { "props": { "style": { "width": "100%", "height": "100%", "backgroundColor": "#222" }}}
import React, { useState } from 'react';

import InputNumber from './index';
import ThemeProvider from '~/contexts/Theme';
import FlexColumn from '~/layout/FlexColumn';

const [status, setStatus] = useState('focus')
const [value, setValue] = useState(5)

const onChange = (newValue) => {
  if (!isNaN(newValue)) {
    setValue(newValue)
    setStatus('focus')
  } else {
    setStatus('busy')
    setValue(value)
  }
}

;<div style={{ padding: 10 }}>
  <InputNumber
    value={value}
    status={status}
    onChange={onChange} />
</div>
```
