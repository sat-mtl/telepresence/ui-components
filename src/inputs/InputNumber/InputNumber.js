import React, { useState, useEffect, useContext } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import StatusEnum from '~/common/StatusEnum'
import { ThemeContext } from '~/contexts/Theme'

import { InputButtonDecrement, InputButtonIncrement } from './InputButton'

/**
 * Gets the class name of the input field
 * @private
 * @param {string} extraClassName - Extra class name of the field
 * @returns {string} The concatenated class name
 */
function getFieldClass (extraClassName) {
  return classNames(
    'InputNumberField',
    extraClassName
  )
}

/**
 * Gets the properties of the input field
 * @private
 * @param {string} className - Extra class name of the field
 * @param {boolean} disabled - Flags if the field is disabled
 * @param {number} step - Range of the decrement or increment actions
 * @param {number} min - Minimal allowed value
 * @param {number} max - Maximum allowed value
 * @param {function} onChange - Function triggered when the input value is changed
 * @param {function} onBlur - Function triggered when the input value is blurred
 * @param {number} value - Current value of the input
 * @returns {object} All properties of the field
 */
function getFieldProps ({ className, disabled, step, min, max, onChange, onBlur, value }) {
  return {
    className: getFieldClass(className),
    disabled: disabled,
    type: 'number',
    step: step,
    min: min,
    max: max,
    onChange: onChange,
    onBlur: onBlur,
    onClick: (e) => { e.target.select() },
    value: value
  }
}

/**
 * Gets the class name of the root tag of the component
 * @private
 * @param {string} theme - Current theme of the component
 * @param {boolean} disabled - Flag if the component is disabled
 * @param {boolean} withButtons - Flag if the buttons are displayed
 * @param {string} status - Status of the component
 * @returns {string} The concatenated class name
 */
function getRootClass ({ theme, disabled, withButtons, status }) {
  return classNames(
    'InputNumber',
    `InputNumber-${theme}`,
    { 'InputNumber-disabled': disabled },
    { 'InputNumber-without-buttons': !withButtons },
    { [`InputNumber-${status}`]: status && !disabled }
  )
}

/**
 * Gets the properties of the wrapper of the input field
 * @private
 * @param {function} onKeyDown - Callback triggered when the user presses a key in the field
 * @param {function} onBlur - Callback triggered when the component is blurred
 * @returns {object} All properties of the field
 */
function getWrapperProps ({ onKeyDown, onBlur }) {
  return {
    className: 'InputNumberFieldWrapper',
    onKeyDown: onKeyDown,
    onBlur: onBlur
  }
}

/**
 * InputNumber is a Controlled Component which lets users edit numeric values in steps
 * @see See [Controlled Components](https://reactjs.org/docs/forms.html#controlled-components) specification
 */
function InputNumber (props) {
  const { withButtons, value } = props
  const [inputedValue, changeInputedValue] = useState(value)

  const theme = useContext(ThemeContext)

  // forces re-rendering from props changes
  useEffect(() => { changeInputedValue(value) }, [value])

  const onFieldChange = (e) => changeInputedValue(e.target.value)

  const fieldProps = getFieldProps({
    ...props,
    onChange: onFieldChange,
    value: inputedValue
  })

  return (
    <div className={getRootClass({ ...props, theme })}>
      {!withButtons ? null : <InputButtonDecrement {...props} />}
      <div {...getWrapperProps(props)}>
        <input {...fieldProps} />
      </div>
      {!withButtons ? null : <InputButtonIncrement {...props} />}
    </div>
  )
}

InputNumber.propTypes = {
  /** Class name for the HTML input */
  className: PropTypes.string,

  /** If `true`, the input will be disabled */
  disabled: PropTypes.bool,

  /** Status of the input */
  status: PropTypes.oneOf(Object.values(StatusEnum)),

  /** If `false` all buttons will be hidden */
  withButtons: PropTypes.bool,

  /** Minimal allowed value */
  min: PropTypes.number,

  /** Maximum allowed value */
  max: PropTypes.number,

  /** Callback triggered when value is changed */
  onChange: PropTypes.func,

  /** Callback triggered when the input is clicked */
  onClick: PropTypes.func,

  /** Callback triggered when the user presses a key in the input */
  onKeyDown: PropTypes.func,

  /** Callback triggered when the input is blurred */
  onBlur: PropTypes.func,

  /** Callback triggered when the input is validated */
  onValidate: PropTypes.func,

  /** Range of the decrement or increment actions */
  step: PropTypes.number,

  /** Default input content */
  value: PropTypes.number
}

InputNumber.defaultProps = {
  disabled: false,
  withButtons: true,
  min: -Infinity,
  max: Infinity,
  onChange: Function.prototype,
  onKeyDown: Function.prototype,
  onBlur: Function.prototype,
  step: 1,
  value: 0
}

export default InputNumber
