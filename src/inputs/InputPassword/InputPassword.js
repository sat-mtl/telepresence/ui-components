import React, { useState, useContext } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

import Icon from '~/common/Icon'
import Button from '~/common/Button'

import StatusEnum from '~/common/StatusEnum'
import { ThemeContext } from '~/contexts/Theme'
import InputText from '~/inputs/InputText'

/**
 * Gets the class name of the root component
 * @private
 * @param {string} theme - Current theme of the component
 * @returns {string} The concatenated class name
 */
function getRootClass (theme) {
  return classNames(
    'InputPassword',
    `InputPassword-${theme}`
  )
}

/**
 * Gets all password props of the input component
 * @private
 * @param {boolean} isDisplayed - Flags if it displays the password
 * @param {number} maxLength - Maximum length allowed
 * @param {number} minLength - Minimum length allowed
 * @param {RegExp} pattern - RegExp pattern of the password
 * @param {boolean} disabled - Flags a disabled input
 * @param {number} size - Indicates how many characters wide the input field should be
 * @returns {object} All properties of the input
 */
function getPasswordProps ({ isDisplayed, maxLength, minLength, pattern, disabled, size }) {
  return {
    type: !disabled && isDisplayed ? 'text' : 'password',
    maxLength: maxLength,
    minLength: minLength,
    pattern: pattern,
    readOnly: disabled,
    size: size
  }
}

/**
 * Gets the properties of the button component
 * @private
 * @param {boolean} disabled - Flags a disabled input
 * @param {boolean} isDisplayed - Flags if it displays the password
 * @param {function} toggleDisplay - Callback that toggles the password display
 * @returns {object} All properties of the button
 */
function getToggleButtonProps ({ disabled, isDisplayed, toggleDisplay }) {
  return {
    className: 'InputPasswordToggle',
    type: 'subtle',
    onClick: () => {
      if (!disabled) {
        toggleDisplay(!isDisplayed)
      }
    }
  }
}

/**
 * Gets the type of the toggle icon
 * @private
 * @param {boolean} isDisplayed - Flags if it displays the password
 * @returns {string} The type of the icon
 * @todo: revisit when all icons are homogenized
 */
function getToggleIconType (isDisplayed, theme) {
  const eyeIcon = theme === 'gabrielle' ? 'formEye' : 'eye'
  return isDisplayed ? 'eye-blind' : eyeIcon
}

/**
 * InputPassword is a Controlled Component which lets users enter and edit a password
 * @see See [Controlled Components](https://reactjs.org/docs/forms.html#controlled-components) specification
 */
function InputPassword (props) {
  const theme = useContext(ThemeContext)
  const [isDisplayed, toggleDisplay] = useState(false)
  const buttonProps = getToggleButtonProps({ ...props, isDisplayed, toggleDisplay })

  const inputProps = {
    ...props,
    rootClassName: getRootClass(theme),
    inputProps: getPasswordProps({ ...props, isDisplayed }),
    addonAfter: (
      <Button {...buttonProps}>
        <Icon type={getToggleIconType(isDisplayed, theme)} />
      </Button>
    )
  }

  return (
    <InputText {...inputProps} />
  )
}

InputPassword.propTypes = {
  /** Class name for the HTML input */
  className: PropTypes.string,

  /** If `true`, the input is disabled */
  disabled: PropTypes.bool,

  /** Hide the extra Switch that displays or hide the password */
  hideSwitch: PropTypes.bool,

  /** Callback fired when the input is blurred */
  onBlur: PropTypes.func,

  /** Callback fired when the value is changed */
  onChange: PropTypes.func,

  /** Callback fired when the input is clicked */
  onClick: PropTypes.func,

  /** Callback fired when the input is focused */
  onFocus: PropTypes.func,

  /** Callback fired when the user presses enter in the input */
  onPressEnter: PropTypes.func,

  /** Short hint displayed in the empty input */
  placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),

  /** Value of the input */
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),

  /** The maximum number of characters the user can input */
  maxLength: PropTypes.number,

  /** The minimum number of characters the user can input */
  minLength: PropTypes.number,

  /** It is a regular expression that the input's value must match */
  pattern: PropTypes.instanceOf(RegExp),

  /** If `true`, this field cannot be edited by the user */
  readOnly: PropTypes.bool,

  /** Indicates how many characters wide the input field should be */
  size: PropTypes.number,

  /** Status of the input */
  status: PropTypes.oneOf(Object.values(StatusEnum))
}

InputPassword.defaultProps = {
  disabled: false,
  hasError: false,
  onBlur: Function.prototype,
  onChange: Function.prototype,
  onClick: Function.prototype,
  value: '',
  placeholder: 'Input a password...'
}

export default InputPassword
