<!-- markdownlint-disable MD041 -->

The `InputPassword` is based on :
  * The `InputText` component with
  * a `Button` as an addon
  * and all the [the specific properties of an input with a password](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/password)

```js { "props": { "style": { "width": "100%", "height": "100%", "backgroundColor": "#222" }}}
import React, { useState } from 'react'
import InputPassword from './InputPassword.js'

import FlexRow from '~/layout/FlexRow'
import StatusEnum from '~/common/StatusEnum'

const [input, setInput] = useState(undefined);
const [isDanger, setDanger] = useState(false);

const blurHandle = (e) => e.target.value === '' ? setDanger(true) : setDanger(false)

;<div style={{ padding: 10 }} >
  <InputPassword
    placeholder='A password is required'
    status={isDanger ? 'danger' : 'focus'}
    value={input}
    onChange={(e) => setInput(e.target.value)}
    onBlur={(e) => setDanger(e.target.value === '')} />
</div>
```
