<!-- markdownlint-disable MD041 -->

The property `side` will change the side of the tooltip.

```js
import React, { useState } from 'react';
import Tooltip from './index'
import FlexRow from '~/layout/FlexRow'

function Message () {
  return (
    <div style={{ width: '290px', height: '95px' }}>
      Hello, I am a tooltip
      <br/>
      All my content can be customized ;)
    </div>
  )
}

<div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
  <Tooltip side='top' content={<Message />}>Top</Tooltip>
  <Tooltip content={<Message />}>Right</Tooltip>
  <Tooltip side='bottom' content={<Message />}>Bottom</Tooltip>
  <Tooltip side='left' content={<Message />}>Left</Tooltip>
</div>
```

The tooltip can be displayed by:

* Hovering the wrapped element (default behaviour), it can be changed by setting the property `visibleOnHover` to `false`.
* Setting the `visible` property to `true`.

```js
import React, { useState } from 'react';
import Tooltip from './index'
import FlexRow from '~/layout/FlexRow'

function Message () {
  return (
    <div style={{ width: '290px', height: '95px' }}>
      Hello, I am a tooltip
      <br/>
      All my content can be customized ;)
    </div>
  )
}

<div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
  <Tooltip visibleOnHover={false} content={<Message />}>Never</Tooltip>
  <Tooltip side='left' visible={true} content={<Message />}>Always</Tooltip>
</div>
```
