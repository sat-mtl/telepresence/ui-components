import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import { ThemeContext } from '~/contexts/Theme'

/**
 * Tooltip component is a text popup tip
 * @visibleName Tooltip
 */
function Tooltip ({ id, children, content, side, visibleOnHover, visible }) {
  const theme = useContext(ThemeContext)

  return (
    <div id={id} className={cn('Tooltip', `Tooltip-${theme}`)}>
      <div className={cn('target', { 'target-default': visibleOnHover })}>
        {children}
      </div>
      <div className={cn('content', `content-${side}`, { 'content-visible': visible })}>
        {content}
      </div>
    </div>
  )
}

Tooltip.propTypes = {
  /** ID of the tooltip */
  id: PropTypes.string,
  /** Element targeted by the tooltip */
  children: PropTypes.node,
  /** Content of the tooltip */
  content: PropTypes.node,
  /** Set the side of the tooltip */
  side: PropTypes.oneOf(['top', 'right', 'bottom', 'left']),
  /** Add the hovering behaviour */
  visibleOnHover: PropTypes.bool,
  /** Force the visibility of the tooltip */
  visible: PropTypes.bool
}

Tooltip.defaultProps = {
  id: null,
  children: null,
  content: null,
  side: 'right',
  visibleOnHover: true,
  visible: false
}

export default Tooltip
