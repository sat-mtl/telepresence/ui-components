import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import StatusEnum from '~/common/StatusEnum'
import Icon from '~/common/Icon'
import Button from '~/common/Button'
import { ThemeContext } from '~/contexts/Theme'

/**
 * Displays a notification message
 * @visibleName Notification
 */
function Notification ({ id, message, description, status, onClose, onClick }) {
  const isClickable = typeof onClick === 'function'
  const isClosable = typeof onClose === 'function'
  const theme = useContext(ThemeContext)

  let $close

  const notificationProps = {
    onClose: isClosable ? onClose : Function.prototype,
    onClick: isClickable ? onClick : Function.prototype,
    className: cn(
      'Notification',
      `Notification-${theme}`,
      `Notification-${status}`,
      { 'Notification-clickable': isClickable }
    )
  }

  if (isClosable) {
    $close = (
      <Button className='close' shape='circle' type='subtle' onClick={onClose}>
        <Icon type='close' />
      </Button>
    )
  }

  return (
    <div id={id} {...notificationProps}>
      <div className={cn('status', `status-${status}`)} />
      <div className='message'>
        {message}
      </div>
      {description && (
        <div className='description'>
          {description}
        </div>
      )}
      {$close}
    </div>
  )
}

Notification.propTypes = {
  /** ID of the notification */
  id: PropTypes.string,
  /** Message of the notification */
  message: PropTypes.node,
  /** Description of the notification */
  description: PropTypes.node,
  /** Status of the notification */
  status: PropTypes.oneOf(Object.values(StatusEnum)),
  /** Function triggered when the notification is closed */
  onClose: PropTypes.func,
  /** Function triggered when the notification is clicked */
  onClick: PropTypes.func
}

Notification.defaultProps = {
  id: null,
  message: null,
  description: null,
  status: StatusEnum.FOCUS,
  onClose: null,
  onClick: null
}

export default Notification
