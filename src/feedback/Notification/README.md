<!-- markdownlint-disable MD041 -->

There is four kinds of notifications mapped on the `StatusEnum`.

```js
import React, { useState } from 'react'
import Notification from './index'
import FlexColumn from '~/layout/FlexColumn'

function Message ({ status }) {
  return (
    <>{`Hello, I am a ${status} notification`}</>
  )
}

function Description () {
  return (
    <>This is a very long description in order to test the multiline description.</>
  )
}

<FlexColumn rowGap={4}>
  <Notification message={<Message status='focus' />} description={<Description />} />
  <Notification status='active' message={<Message status='active' />} description={<Description />} />
  <Notification status='busy' message={<Message status='busy' />} description={<Description />} />
  <Notification status='danger' message={<Message status='danger' />} description={<Description />} />
</FlexColumn>
```

Two events can be used to let the user interact with the notification:

* `onClose` will add a cross button with the `onClick` event.
* `onClick` will add the `onClick` event on the notification.
* When both events are used, the `onClose` event triggers the `onClick` event without stopping the propagation.

```js
import React, { useState } from 'react'
import Notification from './index'
import FlexColumn from '~/layout/FlexColumn'

function Message ({ event }) {
  return (
    <>{`I am a notification with ${event}`}</>
  )
}

const props = {
  description: 'This is a description.'
};

<FlexColumn rowGap={4}>
  <Notification onClick={() => console.log('onClick')} message={<Message event='onClick' />} {...props} />
  <Notification onClose={() => console.log('onClose')} message={<Message event='onClose' />} {...props} />
  <Notification
    onClick={() => console.log('onClick')}
    onClose={(e) => { console.log('onClose'); e.stopPropagation() }}
    message={<Message event='both' />}
    {...props}
  />
</FlexColumn>
```

You can test to pop every notifications you want here:

```js { "props": { "style": { "width": "100%", "height": "100%", "backgroundColor": "#222" }}}
import React, { useState, useRef } from 'react'
import Notification from './index'
import FlexColumn from '~/layout/FlexColumn'

import Button from '~/common/Button'
import Field from '~/inputs/Field'
import InputText from '~/inputs/InputText'

function Form ({ onPush }) {
  const textInput = useRef(null)
  const textArea = useRef(null)

  return (
    <form onSubmit={() => false}>
      <Field title="Notification title">
        <input placeholder="Try Me" ref={textInput} />
      </Field>
      <Field title="Notification content">
        <textarea  placeholder="Try Me" ref={textArea} />
      </Field>
      <Field>
        <Button onClick={() => onPush(
          textInput.current.value,
          textArea.current.value
        )}>
          Push notification
        </Button>
      </Field>
    </form>
  )
}

function Overlay ({ onPop, notifications = [] }) {
  return (
    <div
      id="NotificationOverlay"
      style={{
        position: "fixed",
        top: "4px",
        right: "4px",
        width: "260px",
        height: "100%",
        pointerEvents: "none"
      }}
    >
      <FlexColumn rowGap={4}>
        {notifications.map((n, i) => (
          <div style={{ pointerEvents: "fill" }}>
            <Notification
              key={`${i}`}
              id={`${i}`}
              onClick={onPop}
              {...n}
            />
          </div>
        ))}
      </FlexColumn>
    </div>
  )
}

function Center () {
  const [notifications, setNotifications] = useState([])
  const pushNotification = notif => setNotifications(state => [...state, notif])
  const cleanNotifications = () => setNotifications([])

  return (
    <>
      <Form onPush={(message, description) => pushNotification({ message, description })} />
      <Overlay onPop={() => cleanNotifications()} notifications={notifications} />
    </>
  )
}

<Center />
```
