import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { createUseStyles } from 'react-jss'

import Backdrop from '~/layout/Backdrop'
import ThemeProvider, { ThemeContext } from '~/contexts/Theme'

const useStyles = createUseStyles({
  modalWrapper: {
    width: props => `${props.width}px`
  },
  raised: {
    zIndex: props => props.zIndex || 200
  }
})

/**
 * Modal component used to display dialogs
 * @visibleName Modal
 */
function Modal ({ id, children, onBackdropClick, visible, footer, header, zIndex, width }) {
  const theme = useContext(ThemeContext)
  const classes = useStyles({ zIndex, width })

  const cn = classNames('Modal', `Modal-${theme}`, classes.raised)
  const wrapperCn = classNames('modal-wrapper', classes.modalWrapper)

  if (!visible) return null

  return (
    <div id={id} className={cn}>
      <Backdrop onClick={onBackdropClick} zIndex={zIndex} />
      <div className={wrapperCn}>
        <ThemeProvider value={theme}>
          {header && (<header>{header}</header>)}
          <main>{children}</main>
          {footer && (<footer>{footer}</footer>)}
        </ThemeProvider>
      </div>
    </div>
  )
}

Modal.propTypes = {
  /** ID of the modal */
  id: PropTypes.string,
  /** Body content of the modal */
  children: PropTypes.node,
  /** Function triggered when the modal backdrop is clicked */
  onBackdropClick: PropTypes.func,
  /** Define if the modal is visible */
  visible: PropTypes.bool,
  /** Footer content of the modal */
  footer: PropTypes.node,
  /** Header content of the modal */
  header: PropTypes.node,
  /** Define the z-index CSS property of the modal */
  zIndex: PropTypes.number,
  /** Define the width of the modal */
  width: PropTypes.number
}

Modal.defaultProps = {
  id: '',
  children: null,
  onBackdropClick: Function.prototype,
  visible: false,
  footer: null,
  header: null,
  zIndex: 200,
  width: 520
}

export default Modal
