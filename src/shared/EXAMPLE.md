<!-- markdownlint-disable MD041 -->

```js
import FlexColumn from '~/layout/FlexColumn'
import FlexRow from '~/layout/FlexRow'

import Tooltip from '~/feedback/Tooltip';

import RowHead from '~/shared/RowHead';
import ColumnHead from '~/shared/ColumnHead';
import ConnectionCell from '~/shared/ConnectionCell';
import ShmdataCell from '~/shared/ShmdataCell';
import ColumnSeparator from '~/shared/ColumnSeparator';

import ThemeProvider from '~/contexts/Theme';
import ConnectionStatus from '~/contexts/ConnectionStatus'

import AudioIcon from '../../assets/icons/scenic/audio.svg';
import VideoIcon from '../../assets/icons/scenic/video.svg';

const { ARMED, CONNECTED } = ConnectionStatus;

function Popover () {
  return (
    <div style={{ width: '290px', height: '95px' }}>
      Extra information
    </div>
  )
}

function Matrix ({ borderGap }) {
  return (
    <div classtitle='matrix' style={{ paddingTop: '100px' }}>
      <FlexRow columnGap={borderGap} >
        <FlexColumn rowGap={borderGap}>
          <div style={{ width: '200px', height: '40px' }} />
          <RowHead
            started
            title='Video Input 1'
            subtitle='Normal'
          />
          <RowHead
            started
            title='Video Input 2'
            subtitle='Normal'
          />
          <RowHead
            started
            title='Video Input 3'
            subtitle='Normal'
          />
          <RowHead
            started
            title='Video Input 4'
            subtitle='Normal'
          />
          <RowHead
            started
            title='Video Input 5'
            subtitle='Normal'
          />
          <RowHead
            started
            title='Video Input 6'
            subtitle='Normal'
          />
        </FlexColumn>
        <FlexColumn rowGap={borderGap}>
          <div style={{ width: '70px', height: '40px' }} />
          <Tooltip side='top' popover={<Popover />}>
            <ShmdataCell icon={<VideoIcon />} />
          </Tooltip>
          <Tooltip side='top' popover={<Popover />}>
            <ShmdataCell icon={<AudioIcon />} />
          </Tooltip>
          <Tooltip side='top' popover={<Popover />}>
            <ShmdataCell src='http://placehold.it/160x120&text=image1' />
          </Tooltip>
          <Tooltip side='top' popover={<Popover />}>
            <ShmdataCell icon={<VideoIcon />} />
          </Tooltip>
          <Tooltip side='top' popover={<Popover />}>
            <ShmdataCell icon={<AudioIcon />} />
          </Tooltip>
          <Tooltip side='top' popover={<Popover />}>
            <ShmdataCell src='http://placehold.it/160x120&text=image1' />
          </Tooltip>
        </FlexColumn>
        <FlexColumn rowGap={borderGap}>
          <FlexRow columnGap={borderGap} >
            <ColumnHead
              started
              title='Moniteur Vidéo'
              subtitle='Normal'
            />
            <ColumnHead
              started
              title='Moniteur Vidéo'
              subtitle='Normal'
            />
            <ColumnHead
              started
              title='Moniteur Vidéo'
              subtitle='Normal'
            />
          </FlexRow>
          <FlexRow columnGap={borderGap}>
            <ConnectionCell status={CONNECTED} />
            <ConnectionCell status={ARMED} />
            <ConnectionCell status={ARMED} />
          </FlexRow>
          <FlexRow columnGap={borderGap}>
            <ConnectionCell />
            <ConnectionCell status={CONNECTED} />
            <ConnectionCell />
          </FlexRow>
          <FlexRow columnGap={borderGap}>
            <ConnectionCell />
            <ConnectionCell />
            <ConnectionCell status={CONNECTED} />
          </FlexRow>
          <FlexRow columnGap={borderGap}>
            <ConnectionCell status={CONNECTED} />
            <ConnectionCell status={CONNECTED} />
            <ConnectionCell />
          </FlexRow>
          <FlexRow columnGap={borderGap}>
            <ConnectionCell disabled />
            <ConnectionCell disabled />
            <ConnectionCell status={ARMED} />
          </FlexRow>
          <FlexRow columnGap={borderGap}>
            <ConnectionCell disabled />
            <ConnectionCell disabled />
            <ConnectionCell status={ARMED} />
          </FlexRow>
        </FlexColumn>
        <FlexColumn>
          <ColumnSeparator label='separator' />
        </FlexColumn>
        <FlexColumn rowGap={borderGap}>
          <FlexRow columnGap={borderGap} >
            <ColumnHead
              title='Moniteur Vidéo'
              subtitle='Normal'
            />
            <ColumnHead
              started
              title='Moniteur Vidéo'
              subtitle='Normal'
            />
          </FlexRow>
          <FlexRow columnGap={borderGap} >
            <ConnectionCell disabled />
            <ConnectionCell />
          </FlexRow>
          <FlexRow columnGap={borderGap} >
            <ConnectionCell disabled />
            <ConnectionCell />
          </FlexRow>
          <FlexRow columnGap={borderGap} >
            <ConnectionCell />
            <ConnectionCell />
          </FlexRow>
          <FlexRow columnGap={borderGap} >
            <ConnectionCell />
            <ConnectionCell />
          </FlexRow>
          <FlexRow columnGap={borderGap} >
            <ConnectionCell status={ARMED} />
            <ConnectionCell />
          </FlexRow>
          <FlexRow columnGap={borderGap} >
            <ConnectionCell status={ARMED} />
            <ConnectionCell status={CONNECTED} />
          </FlexRow>
        </FlexColumn>
      </FlexRow>
    </div>
  )
}

<>
  <h3>Legacy style (Gap value: 1px)</h3>
  <div style={{ width: '100%', height: '500px', backgroundColor: '#3b3b3b' }}>
    <Matrix borderGap={2} />
  </div>
  <br/>
  <h3>Simon style (Gap value: 2px)</h3>
  <div style={{ width: '100%', height: '500px', backgroundColor: '#121212' }}>
    <ThemeProvider value='simon'>
      <Matrix borderGap={1} />
    </ThemeProvider>
  </div>
</>
```
