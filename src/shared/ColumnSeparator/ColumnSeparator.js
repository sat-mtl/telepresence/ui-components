import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { createUseStyles } from 'react-jss'

import { ThemeContext } from '~/contexts/Theme'

const useStyles = createUseStyles({
  topLine: {
    'border-top-color': props => `${props.color}`
  },
  bottomLine: {
    'border-left-color': props => `${props.color}`
  }
})

/** ColumnSeparator separates group of columns */
function ColumnSeparator ({ label, color }) {
  const classes = useStyles({ color })
  const theme = useContext(ThemeContext)

  const rootCn = classNames(
    'column-separator',
    `column-separator-${theme}`
  )

  const topLineCn = classNames(
    'column-separator-top-line',
    classes.topLine
  )

  const bottomLineCn = classNames(
    'column-separator-bottom-line',
    classes.bottomLine
  )

  return (
    <div className={rootCn}>
      <div className='column-separator-top'>
        <div className={topLineCn} />
        <div className='column-separator-label'>{label}</div>
      </div>
      <div className='column-separator-bottom'>
        <div className={bottomLineCn} />
      </div>
    </div>
  )
}

ColumnSeparator.propTypes = {
  /** Label of the new separated group */
  label: PropTypes.string,
  color: PropTypes.string
}

ColumnSeparator.defaultProps = {
  label: '',
  color: '#8e8e8e'
}

export default ColumnSeparator
