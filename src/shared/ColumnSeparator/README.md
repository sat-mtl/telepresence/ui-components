```js
import GroupSeparator from './index';
import ThemeProvider from '~/contexts/Theme';

<div style={{ height: '500px' }}>
  <ThemeProvider value='simon'>
    <GroupSeparator />
  </ThemeProvider>
</div>
```
