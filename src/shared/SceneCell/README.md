The `SceneCell` component is used to have a global overview of the Scene in a Scenic session. Logic can be triggered when the `Cell` is clicked. This logic is handled in the `LivePage` component and it manages the `active` status of the Scene.

### Legacy theme

```js
import React, { useState } from 'react';
import FlexRow from '~/layout/FlexRow'
import ThemeProvider from '~/contexts/Theme';
import SceneCell from '~/shared/SceneCell'

function Component ({ label, active = false, selected = false, disabled = false }) {
  const [isActive, toggleActive] = useState(active);


  return (
    <SceneCell
      key={label}
      active={isActive}
      disabled={disabled}
      onClick={() => { toggleActive(!isActive) }}
    >
      {label}
    </SceneCell>
  )
}

<ThemeProvider value='legacy'>
  <FlexRow flexWrap>
    <Component label='Scene 1' active />
    <Component label='Scene 2' selected />
    <Component label='Scene disabled' disabled />
  </FlexRow>
</ThemeProvider>
```

### Simon theme

```js
import React, { useState } from 'react';
import FlexRow from '~/layout/FlexRow'
import ThemeProvider from '~/contexts/Theme';
import SceneCell from '~/shared/SceneCell'

function Component ({ label, active = false, selected = false, disabled = false }) {
  const [isActive, toggleActive] = useState(active);


  return (
    <SceneCell
      key={label}
      active={isActive}
      disabled={disabled}
      onClick={() => { toggleActive(!isActive) }}
    >
      {label}
    </SceneCell>
  )
}

<ThemeProvider value='simon'>
  <FlexRow flexWrap>
    <Component label='Scene 1' active />
    <Component label='Scene 2' selected />
    <Component label='Scene disabled' disabled />
  </FlexRow>
</ThemeProvider>
```
