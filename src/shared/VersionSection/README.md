# App's version section

```js
import React, { useState } from 'react';
import ThemeProvider from '~/contexts/Theme'

import FlexRow from '~/layout/FlexRow'
import FlexColumn from '~/layout/FlexColumn'
import NpmPackage from '../../../package.json'

import VersionSection from './index';

<div style={{backgroundColor: '#000000', position: 'relative'}}>
<ThemeProvider value='simon'>
  <FlexRow columnGap={2}>
    <FlexColumn rowGap={2}>
      <VersionSection NpmPackage={NpmPackage} repoLink='https://gitlab.com/sat-mtl/telepresence/ui-components' />
    </FlexColumn>
  </FlexRow>
</ThemeProvider>
</div>

```

```js
import React, { useState } from 'react';
import ThemeProvider from '~/contexts/Theme'

import FlexRow from '~/layout/FlexRow'
import FlexColumn from '~/layout/FlexColumn'
import NpmPackage from '../../../package.json'

import VersionSection from './index';

<div style={{backgroundColor: '#000000', position: 'relative'}}>
<ThemeProvider value='gabrielle'>
  <FlexRow columnGap={2}>
    <FlexColumn rowGap={2}>
      <VersionSection NpmPackage={NpmPackage} repoLink='https://gitlab.com/sat-mtl/telepresence/ui-components' />
    </FlexColumn>
  </FlexRow>
</ThemeProvider>
</div>

```
