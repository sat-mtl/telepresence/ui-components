import ShmdataCell from './ShmdataCell'

import './styles.common.scss'
import './styles.simon.scss'
import './styles.legacy.scss'

export default ShmdataCell
