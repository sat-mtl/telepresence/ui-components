# Matrix Connection Box

## Example using flexboxes as matrix layout (column-based)

```js
import React, { useState } from 'react';
import ThemeProvider from '~/contexts/Theme'

import ShmdataCell from './index';

import Icon from '~/common/Icon';

import FlexRow from '~/layout/FlexRow'
import FlexColumn from '~/layout/FlexColumn'

const Cell = ({ disabled, icon, src }) => (
  <div style={{ width: '70px', height: '50px' }}>
    <ShmdataCell disabled={disabled} icon={icon} src={src} />
  </div>
);

const titleStyle = {
  display: 'flex',
  justifyContent: 'center'
};

<FlexRow columnGap={2}>
  <FlexColumn rowGap={2}>
    <div style={titleStyle}>Normal</div>
    <Cell icon={<Icon type='audio' />} />
    <ThemeProvider value='simon'>
      <Cell icon={<Icon type='data' />} />
    </ThemeProvider>
  </FlexColumn>
  <FlexColumn rowGap={2}>
    <div style={titleStyle}>Thumbnail</div>
    <Cell src='http://placehold.it/160x120&text=image1' />
    <ThemeProvider value='simon'>
      <Cell src='http://placehold.it/160x120&text=image1' />
    </ThemeProvider>
  </FlexColumn>
  <FlexColumn rowGap={2}>
    <div style={titleStyle}>Disabled</div>
    <Cell disabled icon={<Icon type='audio' />} />
    <ThemeProvider value='simon'>
      <Cell disabled icon={<Icon type='video' />} />
    </ThemeProvider>
  </FlexColumn>
</FlexRow>
```
