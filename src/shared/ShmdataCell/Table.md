## Example using HTML tables as matrix layout (row-based)

```js
import React, { useState } from 'react';
import ThemeProvider from '~/contexts/Theme'
import ShmdataCell from './index';

import AudioIcon from '../../../assets/icons/scenic/audio.svg';
import VideoIcon from '../../../assets/icons/scenic/video.svg';

const Table = ({ children, style }) => (
  <table style={{ tableLayout: 'fixed', ...style }}>
    {children}
  </table>
);

const Row = ({ children, style }) => (
  <tr style={{ ...style }}>
    {children}
  </tr>
);

const FiftyRow = ({ children }) => (
  <Row style={{ height: '50px' }}>
    {children}
  </Row>
);

const Cell = ({ children }) => (
  <td style={{ width: '70px', height: '50px' }}>
    {children}
  </td>
);

<Table style={{ borderSpacing: '1px' }}>
  <thead style={{ fontSize: '10px', textAlign: 'center' }}>
    <Row>
      <Cell>Normal</Cell>
      <Cell>Thumbnail</Cell>
      <Cell>Disabled</Cell>
    </Row>
  </thead>
  <tbody>
    <FiftyRow>
      <Cell>
        <ShmdataCell icon={<AudioIcon />} />
      </Cell>
      <Cell>
        <ShmdataCell src='http://placehold.it/160x120&text=image1' />
      </Cell>
      <Cell>
        <ShmdataCell disabled icon={<AudioIcon />} />
      </Cell>
    </FiftyRow>
    <FiftyRow>
      <ThemeProvider value='simon'>
        <Cell>
          <ShmdataCell icon={<VideoIcon />} />
        </Cell>
        <Cell>
          <ShmdataCell src='http://placehold.it/160x120&text=image1' />
        </Cell>
        <Cell>
          <ShmdataCell disabled icon={<VideoIcon />} />
        </Cell>
      </ThemeProvider>
    </FiftyRow>
  </tbody>
</Table>
```
