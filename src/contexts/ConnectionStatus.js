/**
 * Enum for all connection statuses
 * @readonly
 * @enum {number}
 */
const ConnectionStatus = Object.freeze({
  NONE: 0,
  ARMED: 1,
  CONNECTED: 2
})

export default ConnectionStatus
