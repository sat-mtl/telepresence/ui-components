const path = require('path')

const __root = path.resolve(__dirname, '..')

module.exports = {
  title: 'SAT Valorisation UI',
  webpackConfig: require('./webpack.config'),
  styleguideDir: `${__root}/static`,
  usageMode: 'expand',
  ignore: ['**/index.js'],
  pagePerSection: true,
  sections: [{
    name: 'Documentation',
    sections: [
      {
        name: 'Conventions',
        content: `${__root}/NAMING_CONVENTIONS.md`,
        description: 'The conventions add a nomenclature and can be used as a glossary for class names'
      }]
  }, {
    name: 'Components',
    sections: [{
      name: 'Inputs',
      components: [
        'Checkbox',
        'Field',
        'InputText',
        'InputNumber',
        'InputPassword',
        'InputSelect',
        'Select',
        'Slider',
        'Switch'
      ].map(c => `${__root}/src/inputs/**/${c}.js`)
    }, {
      name: 'Common',
      components: [
        `${__root}/src/common/Icon/*.js`
      ],
      sections: [{
        name: 'Menus',
        content: `${__root}/src/common/Menu/README.md`,
        components: `${__root}/src/common/Menu/**/*.js`
      }, {
        name: 'Status',
        components: `${__root}/src/common/StatusBar/**/*.js`
      }, {
        name: 'Navigation',
        components: `${__root}/src/common/NavTab/**/*.js`
      }, {
        name: 'Others',
        components: [
          `${__root}/src/common/Modal/*.js`,
          `${__root}/src/common/Preview/*.js`,
          `${__root}/src/common/Button/*.js`,
          `${__root}/src/common/Spinner/*.js`,
          `${__root}/src/common/Tag/*.js`
        ]
      }]
    }, {
      name: 'Feedback',
      components: `${__root}/src/feedback/**/*.js`
    }, {
      name: 'Layout',
      components: `${__root}/src/layout/**/*.js`
    }]
  }, {
    name: 'Integrations',
    sections: [{
      name: 'Components',
      components: `${__root}/src/shared/**/*.js`
    }, {
      name: 'Exemples',
      content: `${__root}/src/shared/EXAMPLE.md`
    }]
  }]
}
