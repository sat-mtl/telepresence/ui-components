// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

const path = require('path')

const MINIMAL_COVERAGE = {
  branches: 80,
  functions: 80,
  lines: 80,
  statements: 80
}

const JEST_CONFIG = {
  rootDir: path.resolve(__dirname, '..'),

  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // Indicates whether the coverage information should be collected while executing the test
  collectCoverage: true,

  // The directory where Jest should output its coverage files
  coverageDirectory: '<rootDir>/coverage',

  // This will be used to configure minimum threshold enforcement for coverage results
  coverageThreshold: {
    'src/': MINIMAL_COVERAGE
  },

  // An array of regexp pattern strings that are matched against all file paths before executing the test
  coveragePathIgnorePatterns: [
    '/node_modules/',
    'test/emptyComponent.js',
    'index.js'
  ],

  // A map from regular expressions to module names that allow to stub out resources with a single module
  moduleNameMapper: {
    '\\.(css|scss|jpg|png|html|ejs)$': '<rootDir>/test/emptyModule.js',
    '\\.(svg)$': '<rootDir>/test/emptyComponent.js',
    '^~test(.*)': '<rootDir>/test/$1',
    '^~(.*)': '<rootDir>/src$1',
    '^@assets(.*)': '<rootDir>/assets$1'
  },

  // An array of regexp pattern strings, matched against all module paths before considered 'visible' to the module loader
  modulePathIgnorePatterns: ['node_modules'],

  // The paths to modules that run some code to configure or set up the testing environment before each test
  setupFiles: ['<rootDir>/config/enzyme.setup.js'],

  // The test environment that will be used for testing
  testEnvironment: 'jsdom'
}

if (process.env.CI) {
  // A list of reporter names that Jest uses when writing coverage reports
  JEST_CONFIG.coverageReporters = [
    'text-summary',
    'html'
  ]
}

module.exports = JEST_CONFIG
