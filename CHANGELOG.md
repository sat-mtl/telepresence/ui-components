Release Notes
===================

ui-components 0.2.2 (2022-12-01)
----------------------------------

* ✨ Add qualifier to drawer and modal wrapper ([!139](https://gitlab.com/sat-mtl/tools/ui-components/-/merge_requests/139))

ui-components 0.2.1 (2022-09-02)
----------------------------------

* 📦 Change the organisation for sat-mtl ([!133](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/133))
* 🐛 Fix simon styles for inputPassword ([!136](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/136))
* 💄 Fix styles of the inputText to adapt to simon styles ([!135](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/135))
* 💄 Fix the border-radius of all simon buttons ([!134](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/134))

UI Components 0.2.0 (2022-08-09)
----------------------------------

* 🚑️ Review and fix possible naming conflicts regarding icons of scenic and scenic light ([!129](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/129))
* 🚑️ Review and fix the broken styles for simon inputText components ([!130](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/130))
* 🚨 lint and format all scss files ([!122](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/122))
* 📝 update issue templates ([!121](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/121))
* 🚑️ Resolve critical errors when ui-components is packages for external projects ([!97](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/97))
* ♻️ Update FlexRow and FlexColumn with row-gap and column-gap properties ([!123](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/123))
* 🐛 Fix broken disabled button styles ([!124](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/124))
* 🐛 Fix broken styles for button square ([!126](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/126))
* ✨ Implement a common i18n configuration component ([!120](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/120))
* 🐛 Make the VersionSection available in develop ([!119](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/119))
* 🔥 Remove the StatusBar component and the related test files ([!117](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/117))
* 📦️ Add the svg package ([!118](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/118))
* ✨ Implement the version section as a ui-component ([!115](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/115))
* 📝 Fix minor typos ([!114](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/114))
* 💄 Add the scene screen icon of the preset scene schema ([!113](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/113))
* 💄 Add the preset scene schema icons to the list of icons ([!112](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/112))
* 💄 Adapt the inputPassword icon for gabrielle styles ([!111](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/111))
* 💄 Add the form eye icon to the scenic light icons list ([!110](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/110))
* 💄 Add the Welcome Hub icons to the list of icons ([!109](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/109))
* 🧑‍💻 Update the issue templates ([!107](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/107))
* 🐛 Return the value of the selected option in InputSelect ([!108](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/108))
* ✨ Adapt the checkbox component to Scenic Light styles ([!106](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/106))
* ♻️ Change the Button class names for Pascal Case `Button` ([!93](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/93))
* 🐛 Fix the react styleguidist build ([!105](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/105))
* 🐛 Fix naming conflicts for InputSelectAddon ([!103](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/103))
* 💄 Add all Preset icons to the scenic-light folder in assets ([!102](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/102))
* 💄 Fix margin discrepancies between InputText and InputSelect components ([!100](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/100))
* 🐛 Make the label property optional in the InputSelect component ([!101](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/101))
* 🔥 Remove code `for demonstration purposes` in InputSelect component ([!99](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/99))
* 🐛 Display border-bottom for inputText in gabrielle styles ([!98](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/98))
* ✨ Implement the ScenicLight's Form Select component ([!96](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/96))
* ✨ Implement the form Input text component ([!95](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/95))
* 💄 Improve gabrielle styles when the Button are in active state ([!91](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/91))
* 💄 Update the font-faces of Roboto ([!92](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/92))
* 🧑‍💻 Add Feature Request and Bug Resolution Request templates ([!90](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/90))
* 💄 Add SCCS variables for ScenicLight's colors ([!89](https://gitlab.com/sat-mtl/telepresence/ui-components/-/merge_requests/89))

@sat-valorisation/ui-components 0.1.9 (2022-01-14)
----------------------------------

* ✨ Add onClick handler for Tag components
* ✨ Add INACTIVE status in StatusEnum
* ✨ Add the notification ui-component
* ✨ Add the new tooltip ui-component
* ✨ Add the Tag component and status for RowHead and ColumnHead
* ✨ Add a name property for the inputs
* ✨ Add menu with fields
* ✨ Add an ID for all feedback components
* ✨ Add new InputPassword component
* ✨ Add Button's custom className and InputText's onBlur and onPressEnter callbacks
* ✨ Add and export the column separator component
* ✨ Add circle checkbox
* ✨ Give an id for flexbox
* ✨ Add Checkbox component and active status for Tab
* ✨ Add simon style for the NavTab bar* 🐛 Fix ui-component build with Webpack 5 and peer dependencies
* ✨ Implement StatusBar with simon style
* ✨ New Slider React component
* ✨ Add simon style for all scenic Inputs
* ✨ Add React component for Buttons with Simon style
* ✨ Add React component for all Icons
* ⚡ Add React components for feedbacks
* 🐛 Fix svg loading without styles
* 🐛 Remove the tab index attribute from menu
* 🐛 Fix the FormGroup onSubmit trigger
* 🐛 Expand menus with CSS instead of not loading sub menus
* 🐛 Export the Tag component
* 🐛 Fix modal and drawer z-index conflict
* 🐛 Fix size of rectangular switches
* 🐛 Fix Slider sliding with Chrome
* 📦 Update packages and audit security
* 📦 Upgrade ui-components to webpack 5
* 💚 Add a cache in the ui-components CI
* 💚 Build doc for components in CI
* 👷 Add Gitlab coverage from unit tests
* 💄 Remove overflow on the collapsible groupe
* 💄 Improve the rendering of the Notifications
* 💄 Fix CSS styles of the fields
* 💄 Upgrade FormGroups visuals
* 💄 Change the heavy style of Buttons
* 💄 Add CSS hover on shared components
* 💄 Update styles for SourceHead and add icons for ShmdataCell
* 🏗  Add a className property for all the HTML inputs
* 🏗  Improve modal and drawers HTML structures
* 🏗  Improve modal and drawers HTML structures
* 🍱 Add an unlock icon
* 🍱 Add a download icon
* 🍱 Add themes and colors for some icons for SIP calls
* 🍱 Add Simon color palettes as a theme
* 📝 Fix Field description
* 📝 Add a naming convention for class names
* ♻ Refactor the ConnectionBox
* ♻ Refactor backdrop into an independant component
* ♻ Add simon style for the Menu
* ♻ Refactor Slider and InputNumber with new behaviours
* ♻ Refactor ConnectionCell and ShmdataCell with new Cell layout
* ♻ Refactor FlexRow and FlexColumn with standardized FlexBox
* 🔥 Remove hovered logic for head layouts
* 🚚 Move matrix components into shared folder
* 🔊 Change log level when the input normalization failed
* ➕ Export Icon in Common

UI-Components 0.0.2 (2019-12-02)
---------------------------------

* Added GroupSeparator component for matrix
* Added generic CSS Flexbox wrapper for building matrix
* Added new ShmdataCell React component
* Added new RowHead React component

UI-Components 0.0.1 (2019-11-25)
---------------------------------

* :pencil: Update documentation for release 0.0.1
* :pencil: Update README and CONTRIBUTING
* :wrench: Add convenience scripts for documentation
* :pencil2: Fix typo into README titles
* :page_facing_up: Add reference to the respective font licence
* :card_file_box: Augment version and change package scope
* Add all new scenic-components
* Add documentation
* Add LICENSE
* Add FormInput component
* Use es6 export
* Add library configuration into webpack
* Switch public folder for doc
* Fix gitlab-ci
* Add public page
* Add gitlab ci for static pages
* Document the Icon component
* Initial commit: add basic element
